import sys
import os
from datetime import datetime as dt, timedelta
import json
import pandas as pd
import numpy as np
from os.path import abspath
from datetime import datetime
from datetime import date
from datetime import timedelta
import math
import csv
import pysftp
from ftplib import FTP
import shutil
import pandas.io.sql as sqlio

import threading

import ckg.conf.config as cf
import ckg.util.log as log
import ckg.util.util as util
import ckg.util.sf as sf
import ckg.util.ali as ali
import ckg.util.sap as sap
import xml.dom.minidom
import openpyxl
import re


class DataPreprocessing(object):
    '''
    Data pre-processing
    '''
    prog = 'DataPreprocessing.'
    
    def __init__(self):
        module = self.prog+'__init__()'
        
        log.debug(module, 'Start.')

    def retrieve_article_master(self):
        module = self.prog+'retrieve_article_master()'

        log.debug(module, 'Start.')
    
        rc = cf.rc_ok
        
        try:
                    
            con = ali.connect_to_ali()
            if con == None:
                rc = cf.rc_error
            
            sql = f'''
                        SELECT distinct t1.* FROM public.item_master t1, public.ds_dwd_sales_daily t2  where t1.plant='3000'
                            and t1.article_number=t2.articlenumber
                            order by article_number, sku_color, sku_size
                '''                    
            df = sqlio.read_sql_query(sql, con)
            
            dt = util.get_datetime('%Y%m%d%H%M%S', 0)
            file_name = cf.file_article_master.replace('{data}', dt)
             
            df.to_csv('./data/in/data/'+file_name, index=False, header=True)
            
            self.data_files['article_master'] = file_name
            
            con.close()
            
            log.debug(module, 'End.')

        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error
            
        return rc

    def retrieve_soh(self):
        module = self.prog+'retrieve_soh()'

        log.debug(module, 'Start.')
    
        rc = cf.rc_ok
        
        try:
                    
            con = ali.connect_to_ali()
            if con == None:
                rc = cf.rc_error
            
            sql = f'''
                select t.*, to_char(to_date(launch_date, 'YYYY-MM-DD'), 'IYYY') launch_year, to_char(to_date(launch_date, 'YYYY-MM-DD'), 'IW') launch_week from 
                (
                SELECT  t1.country, t1.shopno, t1.articlenumber,   min(t1.date) launch_date FROM public.ds_dwd_soh_daily t1
                where t1.soh!='0'
                group by country,shopno,articlenumber) t
                order by country,shopno, articlenumber

                '''                    
            df = sqlio.read_sql_query(sql, con)
            
            dt = util.get_datetime('%Y%m%d%H%M%S', 0)
            file_name = cf.file_soh.replace('{data}', dt)
             
            df.to_csv('./data/in/data/'+file_name, index=False, header=True)
            self.data_files['soh'] = file_name

            con.close()
            
            log.debug(module, 'End.')

        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error
            
        return rc

    def retrieve_sales(self):
        module = self.prog+'retrieve_sales()'

        log.debug(module, 'Start.')
    
        rc = cf.rc_ok
        
        try:
                    
            con = ali.connect_to_ali()
            if con == None:
                rc = cf.rc_error
            
            dt = util.get_datetime('%Y%m%d%H%M%S', 0)
            sql = f'''
                    SELECT distinct country, brand, year FROM public.ds_dwd_sales_daily order by country, brand, year
                '''                    
            df = sqlio.read_sql_query(sql, con)
            i = 0
            k = 0
            dt = util.get_datetime('%Y%m%d%H%M%S', 0)
            file_name = cf.file_sales.replace('{data}', dt)
             
            df.to_csv('./data/in/data/'+file_name, index=False, header=True)

            for index, row in df.iterrows():
                country = row['country']
                brand = row['brand']
                year = row['year']
                log.debug(module, f"retrieving for {i+1}, {row['country']}, {row['brand']}, {row['year']}")
                
                sql = f'''
                        select * from ds_dwd_sales_daily where country='{country}' and brand='{brand}' and year='{year}' order by country, date, articlenumber 
                    '''
                df_data = sqlio.read_sql_query(sql, con)
                if i == 0:
                    
                    df_data.to_csv('./data/in/data/'+file_name, index=False, header=True)
                else:
                    df_data.to_csv('./data/in/data/'+file_name, mode='a', index=False, header=False)

                if country == 'SINGAPORE':
                    if k == 0:
                        df_data.to_csv('./log/sg_sales.csv', index=False, header=True)
                    else:
                        df_data.to_csv('./log/sg_sales.csv', mode='a', index=False, header=False)
                        
                    k += 1
                    
                i = i + 1
            
            self.data_files['sales'] = file_name

            con.close()
            
        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error
            
        return rc


    def retrieve_shop_master(self):
        module = self.prog+'retrieve_shop_master()'

        log.debug(module, 'Start.')
    
        rc = cf.rc_ok
        
        try:
                    
            con = ali.connect_to_ali()
            if con == None:
                rc = cf.rc_error
            
            sql = f'''
                select * from public.shop_master order by country, plant, store_category, store_name

                '''                    
            df = sqlio.read_sql_query(sql, con)
            
            dt = util.get_datetime('%Y%m%d%H%M%S', 0)
            file_name = cf.file_store_master.replace('{data}', dt)
             
            df.to_csv('./data/in/data/'+file_name, index=False, header=True)
            self.data_files['store_master'] = file_name

            con.close()
            
            log.debug(module, 'End.')

        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error
            
        return rc

    def retrieve_data(self):
        module = self.prog+'retrieve_data()'
    
        log.debug(module, 'Start.')
        
        self.data_files = {}

        rc = cf.rc_ok
        try:
        
            threads = []
            x = threading.Thread(name='retrieve_article_master', target=self.retrieve_article_master)
            threads.append(x)
            x.start()
            
            x = threading.Thread(name='retrieve_shop_master', target=self.retrieve_shop_master)
            threads.append(x)
            x.start()
            
            x = threading.Thread(name='retrieve_soh', target=self.retrieve_soh)
            threads.append(x)
            x.start()
    
            x = threading.Thread(name='retrieve_sales', target=self.retrieve_sales)
            threads.append(x)
            x.start()
            
            for thread in threads:
                log.debug(module, f"thread: {thread} join.")
                thread.join()
                log.debug(module, f"thread: {thread.name} completed.")
        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error

        log.debug(module, 'End.')
            
        return rc
    
    def preprocess_data(self):
        module = self.prog+'main()'
        
        log.debug(module, 'Start.')
        rc = cf.rc_ok
        
        try:
            #- ---------------------------------------------------------------------
            #- Step #0: load product master and store master
            #- ---------------------------------------------------------------------
            df_products = pd.read_csv('./data/in/data/'+self.data_files['article_master'], dtype=str)
            log.debug(module, f"Step #0, products records: {df_products.shape[0]}")
            df_products = df_products.astype(str)
    
            df_stores = pd.read_csv('./data/in/data/'+self.data_files['store_master'], dtype=str)
            log.debug(module, f"Step #0, store records: {df_stores.shape[0]}")
            df_stores = df_stores[['country','shop_no','store_category']]
            df_stores = df_stores.drop_duplicates(keep='last')
            df_stores.reset_index(inplace=True)
            log.debug(module, f"Step #0, store records (removed duplicates): {df_stores.shape[0]}")
            log.debug(module, f"Step #0, df_stores: \n{df_stores.head(3)}")
    
            log.debug(module, 'Step #0, End.')
            
    
            #- ---------------------------------------------------------------------
            #- Step #1: get the launch date min(first sales, first SOH)
            #- ---------------------------------------------------------------------
            log.debug(module, 'Step #1, Start.')
            df_list = []
            for tmp_df in pd.read_csv('./data/in/data/'+self.data_files['sales'], sep=',', iterator=True, chunksize=500000):
                df_list.append(tmp_df)
            df_sales = pd.concat(df_list)
            
            #df_sales = df_sales.sample(n=10000)
            log.debug(module, f"sales records: {df_sales.shape[0]}")
            
            # get the launch date based on '1st sales'
            df_sales = df_sales.merge(df_stores, how='left', left_on=['country', 'shopno'], right_on=['country', 'shop_no']).set_axis(df_sales.index)
            log.debug(module, f"sales records (merged): {df_sales.shape[0]}")
            df_sales = df_sales[df_sales['store_category'].isin(['RETAIL', 'OUTLET', 'ONLINE'])]
            df_sales.reset_index(inplace=True)
            original_sales = df_sales.copy()
            
            log.debug(module, f"df_sales.columns(before drop): \n{df_sales.columns}")
            df_sales.drop(columns=['level_0', 'brand', 'year', 'week', 'storecategory', 'storename', 'launch', 'category', 'color',
                                   'size', 'soldqty', 'soldqtyexreturns', 'totalgrossprice',
                                   'totalnetprice', 'index', 'shop_no', 'store_category'], inplace=True)
            
            log.debug(module, f"sales records (filtered): {df_sales.shape[0]}")
            log.debug(module, f"df_sales: \n{df_sales.head(5)}")
            log.debug(module, f"df_sales.columns: \n{df_sales.columns}")
            log.debug(module, 'groupby().')
            df_sales = df_sales.groupby(by=['country', 'shopno', 'articlenumber'], as_index=False)['date'].agg('min')
            log.debug(module, f"sales records: {df_sales.shape[0]}")
            log.debug(module, f"df_sales: \n{df_sales.head(5)}")
            df_sales.to_csv('./log/df_sales_launch.csv', index=False, header=True)
            log.debug(module, f"df_sales.columns: \n{df_sales.columns}")

            # get the launch date based on '1st SOH'
            df_list = []
            for tmp_df in pd.read_csv('./data/in/data/'+self.data_files['soh'], sep=',', iterator=True, chunksize=500000):
                df_list.append(tmp_df)
            df_soh = pd.concat(df_list)
            log.debug(module, f"soh records: {df_soh.shape[0]}")
            
            df_soh = df_soh.merge(df_stores, how='left', left_on=['country', 'shopno'], right_on=['country', 'shop_no']).set_axis(df_soh.index)
            log.debug(module, f"soh records (merged): {df_soh.shape[0]}")
            df_soh = df_soh[df_soh['store_category'].isin(['RETAIL', 'OUTLET', 'ONLINE'])]
            df_soh.reset_index(inplace=True)
            log.debug(module, f"df_soh.columns(before drop): \n{df_soh.columns}")
            df_soh.drop(columns=['level_0', 'index', 'launch_year', 'launch_week', 'shop_no', 'store_category',], inplace=True)
            
            log.debug(module, f"soh records (filtered): {df_soh.shape[0]}")
            log.debug(module, f"df_soh: \n{df_soh.head(5)}")
            log.debug(module, f"df_soh.columns: \n{df_soh.columns}")
            log.debug(module, 'groupby().')
            df_soh = df_soh.groupby(by=['country', 'shopno', 'articlenumber'], as_index=False)['launch_date'].agg('min')
            log.debug(module, f"soh records: {df_soh.shape[0]}")
            log.debug(module, f"df_soh: \n{df_soh.head(5)}")
            df_soh.to_csv('./log/df_soh_launch.csv', index=False, header=True)
            log.debug(module, f"df_soh.columns: \n{df_soh.columns}")

            # get the final launch date based on '1st Sales' or '1st SOH', whichever is earlier.
            df_sales.columns = ['country','shopno','articlenumber','sales_launch_date']
            
            log.debug(module, f"df_sales: {df_sales.shape[0]}, df_soh: {df_soh.shape[0]}")
            df_launch = df_sales.merge(df_soh, how='left', on=['country','shopno','articlenumber'])
            log.debug(module, f"df_launch: {df_launch.shape[0]}")
            log.debug(module, f"df_launch.columns: \n{df_launch.columns}")
            
            def f_temp(row):
                if pd.isna(row['sales_launch_date']) == False and pd.isna(row['launch_date']) == False:
                    if row['sales_launch_date'] > row['launch_date']:
                        return row['launch_date']
                    else:
                        return row['sales_launch_date']
                else:
                    return row['sales_launch_date']
                
            df_launch['article_launch_date'] = df_launch.apply(f_temp, axis=1)
            df_launch.to_csv('./log/df_temp1_launch.csv', index=False, header=True)
            
            df_launch.drop(columns=['launch_date','sales_launch_date'], inplace=True)
            df_launch.columns = ['country','shopno','articlenumber','launch_date']
            df_launch.to_csv('./log/df_temp2_launch.csv', index=False, header=True)
            
            log.debug(module, f"set 'launch_year'.")
            df_launch['launch_year'] = df_launch.apply(lambda row: str(row['launch_date'][0:4]), axis=1)
            log.debug(module, f"set 'launch_month'.")
            df_launch['launch_month'] = df_launch.apply(lambda row: str(date(int(row['launch_date'][0:4]), int(row['launch_date'][5:7]), int(row['launch_date'][8:10])).strftime('%B')), axis=1)
            log.debug(module, f"set 'launch_weekunch_year'.")
            df_launch['launch_week'] = df_launch.apply(lambda row: str(date(int(row['launch_date'][0:4]), int(row['launch_date'][5:7]), int(row['launch_date'][8:10])).isocalendar()[1]), axis=1)
                                           
            def f_temp(row):
                dt_s = row['launch_date']
                dt_value = datetime.strptime(dt_s, '%Y-%m-%d')
                dt_value_plus_28 = dt_value + timedelta(days=28)
                
                return dt_value_plus_28.strftime('%Y-%m-%d')
            
            df_launch['launch_date_28_days'] = df_launch.apply(f_temp, axis=1)
            
            df_launch.to_csv('./log/df_final_launch.csv', index=False, header=True)

            # get first 28-days sales
            original_sales = original_sales.merge(df_launch, how='left', on=['country', 'shopno', 'articlenumber']).set_axis(original_sales.index)
            log.debug(module, f"original_sales.1: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            original_sales.drop(columns=['level_0', 'index'], inplace=True)
            log.debug(module, f"original_sales.2: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            # tmp_df = df_sales[df_sales['']]
            
            original_sales = original_sales[original_sales['date']<=original_sales['launch_date_28_days']]
            log.debug(module, f"original_sales (filtered): {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            original_sales.to_csv('./log/df_original_sales.csv', index=False, header=True)
            
            original_sales = original_sales[['country','brand','shopno', 'store_category','articlenumber','color','size', 'launch_year','launch_month','launch_week','launch_date_28_days', 'soldqty','soldqtyexreturns','totalgrossprice','totalnetprice']]
            log.debug(module, f"original_sales.3: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            original_sales = original_sales.groupby(by=['country','brand','shopno', 'store_category','articlenumber','color','size', 'launch_year','launch_month','launch_week','launch_date_28_days'], as_index=False).agg({
                'soldqty': 'sum',
                'soldqtyexreturns': 'sum',
                'totalgrossprice': 'sum',
                'totalnetprice': 'sum'
                })
            log.debug(module, f"original_sales.4a: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            
            original_sales['net_soldqty'] = original_sales.apply(lambda row: str(int(row['soldqty'])+int(row['soldqtyexreturns'])), axis=1)
            log.debug(module, f"original_sales.4b: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            original_sales.to_csv('./log/df_original_sales_4a.csv', index=False, header=True)
        
            tmp_columns = ['country', 'brand', 'shopno', 'store_category', 'articlenumber', 'color', 'size', 'launch_year', 'launch_month', 'launch_week', 'net_soldqty']
            original_sales = original_sales[tmp_columns]
            log.debug(module, f"original_sales.4b: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            original_sales.to_csv('./log/df_original_sales_4b.csv', index=False, header=True)


            # add article/SKU attributes
            #article_master_20220131144500.csv
            # attr_list = ['article_number', 'batch','item_group_id','item_class_name_id','item_sub_class_id','item_department_id','item_category_id','item_product_hierarchy_id','item_season_id',
            #         'item_type_id','item_heel_rang_id','launch_id','brand_id','label_id','length','width','height','avg_moving_price','country_origin',
            #         'art_hscode2','art_shoelast','chest','shoulder','sku_base_studs','sku_capsule','sku_capsule_theme','sku_construction','sku_delivery','sku_eucompliance',
            #         'sku_heel_height','sku_hscode1','sku_hscode2','sku_laptop_size','sku_launchdate','sku_launchdate2','sku_max_strap_length','sku_min_strap_length','sku_new_construction',
            #         'sku_outsole_no','sku_pack_cost_hkd','sku_pack_cost_rmb','sku_pack_cost_sgd','sku_pack_cost_usd','sku_reference_material','sku_remark','sku_shoulder_drop_length',
            #         'sku_strap_width','sku_theme','sku_type','sku_wall','sku_wall_theme','sku_weight','sku_pr','sku_pull_tab','sku_material_type','sku_must_buy_seeding','sku_toebox','sku_other_mat3',
            #         'sku_editorials','sku_age','sku_product','sku_material_pattern','sku_product_campaign','sku_other_mat5','sku_bag_charm','sku_theme_sequence','sku_handle_type',
            #         'sku_lining','sku_primary','sku_colourtone','sku_key_feature','sku_embellishment','sku_hierarchy','sku_other_mat1','sku_insole','sku_new_color','sku_category2',
            #         'sku_color','sku_strap_type','sku_eid_ram','sku_heeltype','sku_multifunctional','sku_window','sku_insoletype','sku_heel_mat','sku_material_finish','sku_outsole','sku_other_mat4',
            #         'sku_size','sku_bag_strap_type','sku_compartments','sku_price_point','sku_main_mat','sku_model_campaign','sku_style','sku_closuretype','sku_model','sku_new_launch_code',
            #         'sku_pm_matcode','shoulder_strap_availability','sku_no_of_feature','sku_other_mat2','sku_launch_type','sku_material_print','sku_colourgroup','sku_texture',
            #         'sku_additional_details','sku_bag_flap_lining_material','sku_outsoletype','sku_modif_type','article_depth','article_handle_drop_length','article_height',
            #         'article_matching_code','article_pre_order_end_date','article_pre_order_start_date','article_width','designer_name','shoe_last_note']
                    
            attr_list = ['article_number', 'batch','item_group_id','item_class_name_id','item_sub_class_id','item_department_id','item_category_id','item_product_hierarchy_id','item_season_id',
                    'item_type_id','item_heel_rang_id','launch_id','brand_id','label_id','sku_color','sku_size','sku_main_mat', 'avg_moving_price']
            
            tmp_products_df = df_products[attr_list]
            tmp_products_df.to_csv('./log/tmp_products_df.csv', index=False, header=True)
            
            original_sales = original_sales.merge(tmp_products_df, how='left', left_on=['articlenumber','color','size'], 
                                                  right_on=['article_number','sku_color','sku_size']).drop(columns= ['articlenumber', 'article_number','sku_color','sku_size'])
            log.debug(module, f"original_sales.5: {original_sales.shape[0]}, columns: \n{original_sales.columns}")
            original_sales.to_csv('./log/df_original_sales_5.csv', index=False, header=True)
            

            dt = util.get_datetime('%Y%m%d%H%M%S', 0)
            file_name = cf.file_input_data.replace('{data}', dt)
             
            original_sales.to_csv('./data/in/data/'+file_name, index=False, header=True)

            self.input_data = file_name
            
            f = open('./ckg/conf/config.py', 'r')
            linelist = f.readlines()
            f.close()

            f2 = open('./ckg/conf/config.py', 'w')
            for line in linelist:
                if 'latest_input_file' in line: #         = '2021-08-22T00:45:40.000Z'
                    line = line.replace(line, 'latest_input_file         = \''+file_name+'\'\n')
                f2.write(line)
            f2.close()
            log.debug(module, 'Step #1, End.')
            
            log.debug(module, 'End.')
        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error
            
        return rc
    
            
    def main(self):
        module = self.prog+'main()'
        
        log.debug(module, 'Start.')

        rc = cf.rc_ok
        
        try:
    
    
            log.debug(module, f"calling retrieve_data()")
            rc = self.retrieve_data()
            log.debug(module, f"called retrieve_data() returns: {rc}")
            if rc != cf.rc_ok:
                log.error(module, f"called retrieve_data() returns: {rc}, failed.")
                return rc

            log.debug(module, f"calling preprocess_data()")
            rc = self.preprocess_data()
            log.debug(module, f"called preprocess_data() returns: {rc}")
            if rc != cf.rc_ok:
                log.error(module, f"called preprocess_data() returns: {rc}, failed.")
                return rc
            log.debug(module, f"final data file: {self.input_data}")
            
        
            log.debug(module, 'End.')
        except BaseException as e:
            rc = cf.rc_error
            log.error(module, e.__str__())
        
        return rc
        
         
        
        