import ckg.conf.config as cf
import ckg.util.log as log
import ckg.util.util as util
import ckg.util.sf as sf
import ckg.util.ali as ali
import ckg.util.sap as sap

import pandas  as pd
import numpy   as np
import os
from sklearn.ensemble import RandomForestRegressor
from pandas_profiling import ProfileReport
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

class CKGModel(object):
    def __init__(self):
        
        self.prog = 'CKGModel.'
        
        module = self.prog+'__init__()'
        
        log.debug(module, 'Start.')
    
    def find_input_data_files(self):
        
        all_files = os.listdir("./data/in/data/")
        
        filtered_files = [f for f in all_files if cf.latest_input_file_timestamp in f]
        
        return sorted(filtered_files)
        
        
    def find_input_data_files_for_countries(self, country_list):
        
        all_files = os.listdir("./data/in/data/")
        
        filtered_files = [f for f in all_files if f.startswith('input') and f.split('_')[1] in country_list]
        
        filtered_files = sorted(filtered_files, reverse=True)
        
        cl = country_list
        r = []
        for f in filtered_files:
            if f.split('_')[1] in cl:
                r.append(f)
                cl.remove(f.split('_')[1])
        
        return sorted(r)

    def fillna(self, col):
        col.fillna(col.value_counts().index[0], inplace=True)
        return col
    
    def encode_frequency(self, df, column, target, return_dict, key):
        module = self.prog+'encode_frequency()'
        
        gp_df = df.groupby(column).sum()/df[target].sum()
        
        gp_dict = gp_df.to_dict()
        gp_dict = gp_dict[target]
        
        df[column] = df[column].apply(lambda x: gp_dict[x])
        
        log.debug(module, f"dict for col {column} target {target} is:\n{gp_dict}")
        return_dict[column] = gp_dict
        
    
    def save_model_result(self, country, brand, item_group_id, model_type, model_file, mae, mse):
        module = self.prog+'save_model_result()'
        
        rc = cf.rc_ok
        
        log.debug(module, f"country: {country}, brand: {brand}, item_group: {item_group_id}, model_type: {model_type}, model_file: {model_file}, mae: {mae}, mse: {mse}")
        
        try:
            model_result_df = pd.DataFrame(columns=['country','brand','item_group','model_type','model_file','mae','mse']) 
            if os.path.exists('./data/out/model/models.csv'):
                tmp_df = pd.read_csv('./data/out/model/models.csv', dtype=str)
                model_result_df = model_result_df.append(tmp_df)
            log.debug(module, f"model_result_df: {model_result_df.shape[0]}")
            row_index = -1
            if model_result_df.shape[0]>0:
                
                for index, temp_row in model_result_df.iterrows():
                    if temp_row['country'] == country and temp_row['brand'] == brand and temp_row['item_group'] == item_group_id \
                        and temp_row['model_type'] == model_type:
                            row_index = index
                            log.debug(module, f"found existing row: {row_index}")
                            break
            
            if row_index>=0:
                model_result_df.iloc[row_index]['model_file'] = model_file
                model_result_df.iloc[row_index]['mse'] = mse
                model_result_df.iloc[row_index]['mae'] = mae
            else:
                new_row = {'country': country, 'brand': brand, 'item_group': item_group_id,
                           'model_type': model_type,  'model_file': model_file,  
                           'mae': mse, 
                           'mse': mae}
                model_result_df = model_result_df.append(new_row, ignore_index=True)
                
            model_result_df = model_result_df.sort_values(by=['country', 'brand', 'item_group', 'model_type'], ascending=True)
            model_result_df = model_result_df.reset_index(drop=True)
            model_result_df.to_csv('./data/out/model/models.csv', index=False, header=True)
            
        except BaseException as e:
            log.error(module, e.__str__())
            
            rc = cf.rc_error
            
        return rc

class LightGBM(CKGModel):

    '''
    Data pre-processing
    '''
        
    def __init__(self):
        
        self.prog = 'LightGBM.'
        
        module = self.prog+'__init__()'
        
        log.debug(module, 'Start.')
        
        self.model_id = 'lgbm'

    
class RandomForest(CKGModel):

    '''
    Data pre-processing
    '''
        
    def __init__(self):
        
        self.prog = 'RandomForest.'
        
        module = self.prog+'__init__()'
        
        log.debug(module, 'Start.')
        
        self.model_id = 'rf'
        
class XGBoost(CKGModel):

    '''
    Data pre-processing
    '''
        
    def __init__(self):
        
        self.prog = 'XGBoost.'
        
        module = self.prog+'__init__()'
        
        log.debug(module, 'Start.')
        
        self.model_id = 'xgb'
        
class NN(CKGModel):

    '''
    Data pre-processing
    '''
        
    def __init__(self):
        
        self.prog = 'NN.'
        
        module = self.prog+'__init__()'
        
        log.debug(module, 'Start.')
        
        self.model_id = 'nn'