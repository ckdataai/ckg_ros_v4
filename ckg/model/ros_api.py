import ckg.conf.config as cf
import ckg.util.log as log
import ckg.util.util as util
import ckg.util.ali as ali
import pandas as pd
import glob
import pandas.io.sql as sqlio
import math

import pickle

from tqdm import tqdm

import ckg.model.base_models as base_models

class ROSAPI(object):

    '''
    ROS API
    '''
    
    def __init__(self, country_list, brand_list):
        
        self.prog = 'ROSAPI.'
        
        module = self.prog+'__init__()'
        
        super().__init__()
        
        log.debug(module, 'Start.')
        
        self.country_list = country_list
        self.brand_list = brand_list
        
    
    def retrieve_model(self, model_id, country, brand, item_group_id):
        module = self.prog+'retrieve_model()'
        
        log.debug(module, f"model_id/country/brand/item_group_id: {country}, {brand}, {item_group_id}")
        
        prefix = str(model_id)+'_'+str(country)+'_'+str(brand)+'_'+str(item_group_id)
        model_name = './data/out/model/'+prefix+'_2*.pkl'

        model_file_list = glob.glob(model_name)
        model_file_list = sorted(model_file_list, reverse=True)

        encoder_name = './data/out/model/'+prefix+'_encoder_*.pkl'
        encoder_file_list = glob.glob(encoder_name)
        encoder_file_list = sorted(encoder_file_list, reverse=True)

        model = None
        encoder = None
        if len(model_file_list) > 0:
            model = model_file_list[0]
        if len(encoder_file_list) > 0:
            encoder = encoder_file_list[0]
        if model==None or encoder==None:
            return (None, None)
        else:
            model = model.split('\\')[-1]
            return (model, encoder.split('\\')[-1])
        
    def predict_by_launch(self, model_id, launch_id):
        def encoder_helper(x, encoder):
            val = encoder.get(x)
            val = 0 if val is None else val
            return val

        module = self.prog+'predict_by_launch()'
        
        log.debug(module, f"Start, model_id: {model_id}, launch_id: {launch_id}")
        rc = cf.rc_ok
        
        try:
            
            #1 retrieve all SKU (shoes and bags only)
            con = ali.connect_to_ali()
            if con == None:
                rc = cf.rc_error
            
            sql = f'''
                    SELECT distinct * FROM dw.item_master where plant='3000' and launch_id='{launch_id}' and item_group_id in ('01', '02')
                    order by brand_id, article_number
                '''  
            log.debug(module, f"sql: \n{sql}")
            
            article_df = sqlio.read_sql_query(sql, con)

            article_df.to_csv('./log/api_article.csv', index=False, header=True)
            
            sql = f'''
                    SELECT * FROM dw.shop_master  
                    order by country, brand, shopno, plant
                '''  
            log.debug(module, f"sql: \n{sql}")
            
            shop_df = sqlio.read_sql_query(sql, con)

            shop_df.to_csv('./log/api_store.csv', index=False, header=True)
            
            for c in self.country_list:
                for b in self.brand_list:
                    log.debug(module, f"handle country/brand: {c}, {b}")
                    
                    shop_c_df = shop_df[(shop_df['country']==c)&(shop_df['brand']==b)]
                                            
                    shop_c_df.to_csv('./log/api_store_'+c+'.csv', index=False, header=True)
                    log.debug(module, f"handle country/brand: {c}, {b}, 01.")
                    
                    article_c_df = article_df.copy()
                    article_c_df = article_c_df[article_c_df['brand_id']==b]
    
                    article_c_df = article_c_df[['article_number', 'brand_id','color','size', 'batch', 'barcode', 'item_group_id','item_class_name_id','item_sub_class_id',
                                                 'item_department_id','item_category_id','item_product_hierarchy_id','item_season_id',
                                                 'item_type_id','item_heel_rang_id','launch_id','label_id','sku_main_mat']]
                    log.debug(module, f"handle country: {c}, 02.")
                    log.debug(module, f"handle country: {c}, 02, shop_c_df: {shop_c_df.shape}")
                    
                    append_csv = False
                    i = 0
                    output_data = []
                    for _, row_s in tqdm(shop_c_df.iterrows(), total=len(shop_c_df)):
                        store = row_s['shopno']
                        store_category = row_s['store_category']
                        log.debug(module, f"predict for store: {store}, {store_category}")

                        
                  
                        item_group_ids = list(set(article_c_df['item_group_id']))
                        article_c_df['launch_year'] = '2022'
                        article_c_df['launch_month'] = 'May'
                        article_c_df['launch_week'] = '17'

                        for item_group_id in item_group_ids:
                            df_temp_ = article_c_df[(article_c_df['item_group_id'] == item_group_id)].reset_index(drop=True).copy()
                            if len(df_temp_) == 0:
                                continue
                            else:
                                model_file, encoder_file = self.retrieve_model(model_id, c, b, item_group_id)
                                log.debug(module, f"model_file: {model_file}, encoder_file: {encoder_file}")
                                if model_file == None:
                                    log.error(module, f"no model found.")
                                    break
                                if encoder_file == None:
                                    log.error(module, f"no encoder found.")
                                    break

                                # loading the files
                                model = pickle.load(open('./data/out/model/'+model_file, 'rb'))
                                encoder = pickle.load(open('./data/out/model/'+encoder_file, 'rb'))

                                # Get the shop no from the encoder
                                val = encoder.get('shopno').get(store)
                                val = 0 if val is None else val
                                df_temp_['shopno'] = val

                                val = encoder.get('store_category').get(store_category)
                                val = 0 if val is None else val
                                df_temp_['store_category'] = val

                                
                                result_df = df_temp_[['article_number','batch', 'barcode']].copy()
                                result_df['country'] = c
                                result_df['shop_no'] = store

                                result_df = result_df[['country', 'shop_no', 'article_number','batch', 'barcode']]
                                

                                for fld in ['color', 'size', 
                                    'launch_year',
                                    'launch_month', 'launch_week',
                                    'item_class_name_id',
                                    'item_sub_class_id', 'item_department_id', 'item_category_id',
                                    'item_season_id', 'item_type_id', 'launch_id', 'sku_main_mat']:

                                    df_temp_[fld] = df_temp_[fld].apply(encoder_helper, args = (encoder.get(fld),))
                                    
                                cols =[ 'shopno', 'store_category', 'color', 'size', 'launch_year',
                                'launch_month', 'launch_week', 'item_class_name_id',
                                'item_sub_class_id', 'item_department_id', 'item_category_id',
                                'item_season_id', 'item_type_id', 'launch_id', 'sku_main_mat']
                                df_temp_ = df_temp_[cols]
                                result = model.predict(df_temp_)
                                result_df['Suggested Qty'] = pd.Series(result)
                                result_df['Suggested Qty'] = result_df['Suggested Qty'].apply(lambda x: math.ceil(float(x)))

                    
                                dt = util.get_datetime('%Y%m%d%H', 0)
                                file_name = cf.file_output_file.replace('{country}', c).replace('{brand_id}', b)\
                                                .replace('{launch_id}', launch_id).replace('{data}', dt)
                                # file_name = file_name.replace('{brand_id}', b)
                                # file_name = file_name.replace('{launch_id}', launch_id)
                                # file_name = file_name.replace('{data}', dt)

                                csv_file_name = './data/out/result/'+file_name
                                if append_csv:
                                    result_df.to_csv(csv_file_name, mode = 'a', header=False, index=False)
                                else:
                                    result_df.to_csv(csv_file_name, index=False)
                                    append_csv = True

                                # csv_file = open('./data/out/result/'+file_name, mode='w', newline='')
                                # wr = csv.writer(csv_file)
                                # wr.writerow(['Country','Store','Article','Batch','Barcode','Suggested Qty'])
                                # wr.writerows(output_data)
            
                        """

                        for index_article, row_a in tqdm(article_c_df.iterrows(), total=len(article_c_df)):
                            #during testing, only handle 10 SKUs per store.
            
                            
                            log.debug(module, f"{article_c_df.columns}: {row_a}")
                            log.debug(module, f"brand/item_group_id: {row_a['brand_id']}|{row_a['item_group_id']}")
                            
                            one_row = row_a.copy()
                            
                            #manual set launch date since we do not have for new articles (never sold before)
                            #E1: Summer 2022, I assume it stars from 2022.05.01
                            one_row['launch_year'] = '2022'
                            one_row['launch_month'] = 'May'
                            one_row['launch_week'] = '17'
                            
                            # retrieve the model and encodder
                            model_file, encoder_file = self.retrieve_model(model_id, c, row_a['brand_id'], row_a['item_group_id'])
                            log.debug(module, f"model_file: {model_file}, encoder_file: {encoder_file}")
                            if model_file == None:
                                log.error(module, f"no model found.")
                                break
                            if encoder_file == None:
                                log.error(module, f"no encoder found.")
                                break
                            
                            #load the model
                            model = pickle.load(open('./data/out/model/'+model_file, 'rb'))
    
                            #load the encoder
                            encoder = pickle.load(open('./data/out/model/'+encoder_file, 'rb'))
                            
                            log.debug(module, f"encoder: \n{encoder}")
    
                            X = []
                            
                            #shopno
                            val = encoder.get('shopno').get(store)
                  
                            if val==None:
                                val = 0.0
                            X.append(val)
                            
                            #store_category
                            val = encoder.get('store_category').get(store_category)
                            if val==None:
                                val = 0.0
                            X.append(val)
                            
                            for fld in ['color', 'size', 
                                        'launch_year',
                                        'launch_month', 'launch_week',
                                'item_class_name_id',
                                'item_sub_class_id', 'item_department_id', 'item_category_id',
                                'item_season_id', 'item_type_id', 'launch_id', 'sku_main_mat']:
                                log.debug(module, f"encoding field: {fld}")
                            
                                val = encoder.get(fld).get(one_row[fld])
                                if val==None:
                                    val = 0.0
                                log.debug(module, f"encoding field: {fld}, val: {val}")
                                X.append(val)
                                
                            #'avg_moving_price'
                            #X.append(0.0)
                            X = np.array(X).reshape(1,-1)
                            X_df = pd.DataFrame(X)
                            
                            X_df.columns = [ 'shopno', 'store_category', 'color', 'size', 'launch_year',
                                'launch_month', 'launch_week', 'item_class_name_id',
                                'item_sub_class_id', 'item_department_id', 'item_category_id',
                                'item_season_id', 'item_type_id', 'launch_id', 'sku_main_mat']
                            
                            result = model.predict(X_df)
                            #log.error(module, f"prediction result: {result}")
                            
                            #article_number,batch,barcode
                            one_line = []
                            one_line.append(c)
                            one_line.append(store)
                            one_line.append(one_row['article_number'])
                            one_line.append(one_row['batch'])
                            one_line.append(one_row['barcode'])
                            one_line.append(math.ceil(float(result)))
                            
                            output_data.append(one_line)
                    
                    #export the prediction result
                    #file_output_file    = 'predicted_1st_4wks_sales_{country}_{launch_id}_{data}.csv'
                    dt = util.get_datetime('%Y%m%d%H%M%S', 0)
                    file_name = cf.file_output_file.replace('{country}', c)
                    file_name = file_name.replace('{brand_id}', b)
                    file_name = file_name.replace('{launch_id}', launch_id)
                    file_name = file_name.replace('{data}', dt)
                     
                    csv_file = open('./data/out/result/'+file_name, mode='w', newline='')
                    wr = csv.writer(csv_file)
                    wr.writerow(['Country','Store','Article','Batch','Barcode','Suggested Qty'])
                    wr.writerows(output_data)
                    """
              
            # close DB connection
            con.close()

        except BaseException as e:
            log.error(module, e.__str__())
        
