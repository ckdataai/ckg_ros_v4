import ckg.conf.config as cf
import ckg.util.log as log
import ckg.util.util as util
import ckg.util.sf as sf
import ckg.util.ali as ali
import ckg.util.sap as sap
import pandas as pd
import numpy as np
import os
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

import xgboost as xgb

from lightgbm import LGBMRegressor
from sklearn.model_selection import StratifiedKFold
import optuna
from optuna.integration import LightGBMPruningCallback

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
import kerastuner as kt

import pickle

import ckg.model.base_models as base_models

class ROSLightGBM(base_models.LightGBM):

    '''
    Light GBM
    '''
    
    def __init__(self, data_file):
        
        self.prog = 'ROSLigthGBM.'
        
        module = self.prog+'__init__()'
        
        super().__init__()
        
        log.debug(module, 'Start.')
        
        self.data_file = data_file
        

    # def objective(trial,data=data,target=target):
        
    #     train_x, test_x, train_y, test_y = train_test_split(data, target, test_size=0.2,random_state=42)
    #     param = {
    #         'metric': 'rmse', 
    #         'random_state': 48,
    #         'n_estimators': 20000,
    #         'reg_alpha': trial.suggest_loguniform('reg_alpha', 1e-3, 10.0),
    #         'reg_lambda': trial.suggest_loguniform('reg_lambda', 1e-3, 10.0),
    #         'colsample_bytree': trial.suggest_categorical('colsample_bytree', [0.3,0.4,0.5,0.6,0.7,0.8,0.9, 1.0]),
    #         'subsample': trial.suggest_categorical('subsample', [0.4,0.5,0.6,0.7,0.8,1.0]),
    #         'learning_rate': trial.suggest_categorical('learning_rate', [0.006,0.008,0.01,0.014,0.017,0.02]),
    #         'max_depth': trial.suggest_categorical('max_depth', [10,20,100]),
    #         'num_leaves' : trial.suggest_int('num_leaves', 1, 1000),
    #         'min_child_samples': trial.suggest_int('min_child_samples', 1, 300),
    #         'cat_smooth' : trial.suggest_int('min_data_per_groups', 1, 100)
    #     }
    #     model = LGBMRegressor(**param)  
        
    #     model.fit(train_x,train_y,eval_set=[(test_x,test_y)],early_stopping_rounds=100,verbose=False)
        
    #     preds = model.predict(test_x)
        
    #     rmse = mean_squared_error(test_y, preds,squared=False)
        
    #     return rmse


    # def objective(trial):
    #     data, target = sklearn.datasets.load_breast_cancer(return_X_y=True)
    #     train_x, test_x, train_y, test_y = train_test_split(data, target, test_size=0.25)
    #     dtrain = lgb.Dataset(train_x, label=train_y)
     
    #     param = {
    #         'objective': 'binary',
    #         'metric': 'binary_logloss',
    #         'lambda_l1': trial.suggest_loguniform('lambda_l1', 1e-8, 10.0),
    #         'lambda_l2': trial.suggest_loguniform('lambda_l2', 1e-8, 10.0),
    #         'num_leaves': trial.suggest_int('num_leaves', 2, 256),
    #         'feature_fraction': trial.suggest_uniform('feature_fraction', 0.4, 1.0),
    #         'bagging_fraction': trial.suggest_uniform('bagging_fraction', 0.4, 1.0),
    #         'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
    #         'min_child_samples': trial.suggest_int('min_child_samples', 5, 100),
    #     }
     
    #     gbm = lgb.train(param, dtrain)
    #     preds = gbm.predict(test_x)
    #     pred_labels = np.rint(preds)
    #     accuracy = sklearn.metrics.accuracy_score(test_y, pred_labels)
    #     return accuracy
     
    # study = optuna.create_study(direction='maximize')
    # study.optimize(objective, n_trials=100)
     
    # print('Number of finished trials:', len(study.trials))
    # print('Best trial:', study.best_trial.params)
    


    # def objective(trial, X, y):
    #     param_grid = { "n_estimators": trial.suggest_int("n_estimators", 1, 500, step = 1),
    #                     "learning_rate": trial.suggest_float("learning_rate", 0.001, 0.1),
    #                     "num_leaves": trial.suggest_int("num_leaves", 10, 200, step=1),
    #                     "num_iterations":  trial.suggest_int("num_iterations", 200, 300, step = 1),
    #                     "min_data_in_leaf": trial.suggest_int("min_data_in_leaf", 50, 1000, step=10),
    #                     "max_bin": trial.suggest_int("max_bin", 200, 500),
    #                     "lambda_l1": trial.suggest_int("lambda_l1", 0, 100, step=5),
    #                     "lambda_l2": trial.suggest_int("lambda_l2", 0, 100, step=5),
    #                     "min_gain_to_split": trial.suggest_float("min_gain_to_split", 0, 15),
    #                     "bagging_fraction": trial.suggest_float("bagging_fraction", 0.2, 0.95, step=0.1),
    #                     'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
    #                     "feature_fraction": trial.suggest_float( "feature_fraction", 0.2, 1, step=0.1)
    #                 }

    #     cv = StratifiedKFold(n_splits=n_split, shuffle=True, random_state=42)
    #     cv_scores = np.empty(n_split)

    #     for idx, (train_idx, test_idx) in enumerate(cv.split(X, y)):
    #         X_train, X_test = X.iloc[train_idx], X.iloc[test_idx]
    #         y_train, y_test = y[train_idx], y[test_idx]

    #         model = LGBMRegressor(silent=True, **param_grid)

    #         model.fit(X_train, y_train, eval_set=[(X_test, y_test)], eval_metric="l2",
    #                     early_stopping_rounds=100,
    #                     callbacks=[LightGBMPruningCallback(trial, "l2")],  
    #                     feature_name = feature_list,
    #                     categorical_feature = categorical_feature)
            
    #         pred = model.predict(X_test)
    #         cv_scores[idx] = mean_squared_error(y_test, pred)
        
    #     return np.mean(cv_scores)

    def train(self):
        module = self.prog+'train()'
        
        log.debug(module, f"file: {self.data_file}")
        
        try:
        
            df = pd.read_csv('./data/in/data/'+self.data_file, dtype=str)
            
            df_group = df.copy()
            df_group = df_group[['country','brand','item_group_id']]
            df_group = df_group.drop_duplicates()
            df_group = df_group[df['item_group_id'].isin(['01','02'])]
            df_group.to_csv('./log/mzz.csv', index=False, header=True)
            

            for index, row in df_group.iterrows():
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}")
            
                df_origin = df[(df['country']==row['country'])&(df['brand']==row['brand'])&(df['item_group_id'].isin([row['item_group_id']]))]
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                
                tmp_df = df_origin.copy()
                tmp_df['avg_moving_price'] = tmp_df['avg_moving_price'].astype(float)
                
                target_attribute = tmp_df['net_soldqty']
                target_attribute = target_attribute.astype(int)

                tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
                print(tmp_df.dtypes)
                
                lbl = preprocessing.LabelEncoder()
                for c in tmp_df.columns:
                    #print(f"c: {c}")
                    if c != 'avg_moving_price':
                        lbl.fit(tmp_df[c].astype(str))
                        tmp_df[c] = lbl.transform(tmp_df[c].astype(str)) 
                
                X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
                print(tmp_df.shape[0], X_train.shape[0])
                
                X_train = X_train.reset_index(drop=True)
                X_test = X_test.reset_index(drop=True)
                y_train = y_train.reset_index(drop=True)
                y_test = y_test.reset_index(drop=True)

                # y_train = y_train.reset_index(drop=True)
                # y_test = y_test.reset_index(drop=True)
                
                X_train.to_csv('./log/X_train.csv', index=False, header=True)
                y_train.to_csv('./log/y_train.csv', index=False, header=True)
                
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                
                log.debug(module, f"X_train.columns: \n{list(X_train.columns)}")
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                num_of_folds = 10
                num_of_calls = 1
                def objective(trial, X, y):
                    
                    module = self.prog+'train().objective()'

                    nonlocal num_of_calls
                    log.debug(module, f"Start, {num_of_calls}, X.shape: {X.shape[0]}, y.shape: {y.shape[0]}")
                    
                    num_of_calls= num_of_calls + 1
                    
                    param_grid = { "n_estimators": trial.suggest_int("n_estimators", 1, 500, step = 1),
                                    "learning_rate": trial.suggest_float("learning_rate", 0.001, 0.1),
                                    "num_leaves": trial.suggest_int("num_leaves", 10, 200, step=1),
                                    "num_iterations":  trial.suggest_int("num_iterations", 200, 300, step = 1),
                                    "min_data_in_leaf": trial.suggest_int("min_data_in_leaf", 50, 1000, step=10),
                                    "max_bin": trial.suggest_int("max_bin", 200, 500),
                                    "lambda_l1": trial.suggest_int("lambda_l1", 0, 100, step=5),
                                    "lambda_l2": trial.suggest_int("lambda_l2", 0, 100, step=5),
                                    "min_gain_to_split": trial.suggest_float("min_gain_to_split", 0, 15),
                                    "bagging_fraction": trial.suggest_float("bagging_fraction", 0.2, 0.95, step=0.1),
                                    'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
                                    "feature_fraction": trial.suggest_float( "feature_fraction", 0.2, 1, step=0.1)
                                }
            
                    cv = StratifiedKFold(n_splits=num_of_folds, shuffle=True, random_state=42)
                    cv_scores = np.empty(num_of_folds)
                    
                    y = y.reset_index(drop=True)
                    
                    log.debug(module, f"X.type: {type(X)}")
                    log.debug(module, f"X first 10: \n{X.head(10)}")
                    log.debug(module, f"X last 10: \n{X.tail(10)}")
                    log.debug(module, f"y.type: {type(y)}")
                    log.debug(module, f"y first 10: \n{y.head(10)}")
                    log.debug(module, f"y last 10: \n{y.tail(10)}")
                    kfold_seq = 1
                    for idx, (train_idx, test_idx) in enumerate(cv.split(X, y)):
                        log.debug(module, f"kfold_seq: {kfold_seq}.")
                        kfold_seq= kfold_seq + 1
                        log.debug(module, f"kfold_seq: {kfold_seq}, a1.")

                        X_train, X_test = X.iloc[train_idx], X.iloc[test_idx]
                        log.debug(module, f"kfold_seq: {kfold_seq}, a2.")
                        y_train, y_test = y.iloc[train_idx], y.iloc[test_idx]
                        log.debug(module, f"kfold_seq: {kfold_seq}, a3.")
            
                        model = LGBMRegressor(silent=True, **param_grid)
                        
                        log.debug(module, f"kfold_seq: {kfold_seq}, a4.")
                        
                        
                        X_train = X_train.reset_index(drop=True)
                        X_test  = X_test.reset_index(drop=True)
                        y_train = y_train.reset_index(drop=True)
                        y_test  = y_test.reset_index(drop=True)
                        log.debug(module, f"kfold_seq: {kfold_seq}, a4.1.")
                        log.debug(module, f"kfold_seq: {kfold_seq}, a4.1, \nX_train.columns: {X_train.columns}")
                        log.debug(module, f"kfold_seq: {kfold_seq}, a4.1, \nX_test.columns: {X_test.columns}")

                        model.fit(X_train, 
                                  y_train, 
                                  eval_set    = [(X_test, y_test)], 
                                  eval_metric = 'l2',
                                  early_stopping_rounds=100,
                                  callbacks=[LightGBMPruningCallback(trial, 'l2')],
                                  feature_name = list(X_train.columns),
                                  categorical_feature = list(X_train.columns).remove('avg_moving_price'))
                        log.debug(module, f"kfold_seq: {kfold_seq}, a5.")

                        y_pred = model.predict(X_test)
                        log.debug(module, f"kfold_seq: {kfold_seq}, a6.")
                        cv_scores[idx] = mean_squared_error(y_test, y_pred)
                        log.debug(module, f"kfold_seq: {kfold_seq}, a7.")
                        
                    return np.mean(cv_scores)

                study = optuna.create_study(direction="minimize", study_name="LGBM")
                func = lambda trial: objective(trial, X_train, y_train)
                study.optimize(func, n_trials=100)
                log.debug(module, f"best params: \n {study.best_params}")
                
                log.debug(module, f"Create model.")
                # study.best_params['boosting_type'] = 'gbdt'
                # log.debug(module, f"best params.1: \n {study.best_params}")
                regressor = LGBMRegressor(**study.best_params)
                log.debug(module, f"Train model.")
                regressor.fit(X_train, y_train, feature_name=list(X_train.columns), categorical_feature=list(X_train.columns).remove('avg_moving_price'))
                
                # test
                y_pred = regressor.predict(X_test)
                y_pred = y_pred.round()
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
                log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
                #save the model: '{model_id}_{country}_{brand}_{item_group}_{datetime}.pkl'
                dt = util.get_datetime('%Y%m%d%H%M%S', 0)
                file_name = cf.model_file.replace('{model_id}', self.model_id).replace('{country}', row['country']).replace('{brand}', row['brand']).replace('{item_group}', row['item_group_id'])
                file_name = file_name.replace('{datetime}', dt)
                model_file = open('./data/out/model/'+file_name, 'ab')
                log.debug(module, f"saved model file: {file_name}")
                
                pickle.dump(regressor, model_file)                     
                model_file.close()
                
                #save the model test results
                self.save_model_result(row['country'], row['brand'], row['item_group_id'], self.model_id, file_name,
                                       str(mean_squared_error(y_test, y_pred)), mean_absolute_error(y_test, y_pred))
            
        except BaseException as e:
            log.error(module, e.__str__())
        
        log.debug(module, f"End.")
        
    def predict(self, data):
        module = self.prog+'predict()'
        
        log.debug(module, f"Start.\n{data}")
        
        #data: country, article, color, size
        
        
        try:
            model_file = open('./data/out/model/rf.pkl', 'rb')     
            regressor = pickle.load(model_file)
            
            df = pd.read_csv('./data/in/data/'+cf.latest_input_file, dtype=str)
            
            df_origin = df[(df['country']=='SINGAPORE')& (df['item_group_id'].isin(['01']))]
            
            tmp_df = df_origin.copy()
            target_attribute = tmp_df['net_soldqty']
            tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
            
            X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
            print(tmp_df.shape[0], X_train.shape[0])
            
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            
            lbl = preprocessing.LabelEncoder()
            for c in X_train.columns:
                print(c)
                if c != 'avg_moving_price':
                    X_train[c] = lbl.fit_transform(X_train[c].astype(str)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(str)) 
                else:
                    print(f"set {c} to float.")
                    X_train[c] = lbl.fit_transform(X_train[c].astype(float)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(float)) 
                    
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            # test
            y_pred = regressor.predict(X_test)
            y_pred = y_pred.round()
            log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
            log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
        except BaseException as e:
            pass


class ROSRandomForest(base_models.RandomForest):

    '''
    Random Forest
    '''
    
    def __init__(self, data_file):
        
        self.prog = 'ROSRandomForest.'
        
        module = self.prog+'__init__()'
        
        super().__init__()
        
        log.debug(module, 'Start.')
        
        self.data_file = data_file
        

    def train(self):
        module = self.prog+'train()'
        
        log.debug(module, f"file: {self.data_file}")
        
        try:
        
            df = pd.read_csv('./data/in/data/'+self.data_file, dtype=str)
            
            df_group = df.copy()
            df_group = df_group[['country','brand','item_group_id']]
            df_group = df_group.drop_duplicates()
            df_group = df_group[df['item_group_id'].isin(['01','02'])]
            df_group.to_csv('./log/mzz.csv', index=False, header=True)
            
            
            for index, row in df_group.iterrows():
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}")
            
                df_origin = df[(df['country']==row['country'])&(df['brand']==row['brand'])&(df['item_group_id'].isin([row['item_group_id']]))]
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                
                tmp_df = df_origin.copy()
                target_attribute = tmp_df['net_soldqty']
                tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
                
                X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
                print(tmp_df.shape[0], X_train.shape[0])
                
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                lbl = preprocessing.LabelEncoder()
                for c in X_train.columns:
                    print(c)
                    if c != 'avg_moving_price':
                        X_train[c] = lbl.fit_transform(X_train[c].astype(str)) 
                        X_test[c] = lbl.fit_transform(X_test[c].astype(str)) 
                    else:
                        print(f"set {c} to float.")
                        X_train[c] = lbl.fit_transform(X_train[c].astype(float)) 
                        X_test[c] = lbl.fit_transform(X_test[c].astype(float)) 
                        
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                # Create the parameter grid based on the results of random search 
                # Number of trees in random forest
                n_estimators = [x for x in range(100, 1000, 100)]
                # Number of features to consider at every split
                max_features = ['auto', 'sqrt']
                # Maximum number of levels in tree
                max_depth = [x for x in range(3,10)]
                max_depth.append(None)
                # Minimum number of samples required to split a node
                min_samples_split = [x for x in range(2,10)]
                # Minimum number of samples required at each leaf node
                min_samples_leaf = [x for x in range(1,10)]
                # Method of selecting samples for training each tree
                bootstrap = [True, False]
                # Create the random grid
                param_grid = {'n_estimators': n_estimators,
                               'max_features': max_features,
                               'max_depth': max_depth,
                               'min_samples_split': min_samples_split,
                               'min_samples_leaf': min_samples_leaf,
                               'bootstrap': bootstrap}     
                log.debug(module, f"grid search parameters: \n{param_grid}")
         
                # tuning model
                #regressor = GridSearchCV(estimator = RandomForestRegressor(), param_grid = param_grid, cv = 5, n_jobs = -1, verbose = 2)
                regressor = RandomizedSearchCV(estimator = RandomForestRegressor(random_state=42), 
                                               param_distributions = param_grid, 
                                               n_iter = 10,
                                               random_state = 42,
                                               return_train_score = True,
                                               n_jobs = -1, 
                                               verbose = 2)
                regressor.fit(X_train, y_train)
                log.debug(module, f"after tuning, parameters: \n{regressor.best_params_}")
                
                # test
                y_pred = regressor.predict(X_test)
                y_pred = y_pred.round()
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
                log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
                #save the model: '{model_id}_{country}_{brand}_{item_group}_{datetime}.pkl'
                dt = util.get_datetime('%Y%m%d%H%M%S', 0)
                file_name = cf.model_file.replace('{model_id}', self.model_id).replace('{country}', row['country']).replace('{brand}', row['brand']).replace('{item_group}', row['item_group_id'])
                file_name = file_name.replace('{datetime}', dt)
                model_file = open('./data/out/model/'+file_name, 'ab')
                log.debug(module, f"saved model file: {file_name}")
                
                pickle.dump(regressor, model_file)                     
                model_file.close()
                
                #save the model test results
                self.save_model_result(row['country'], row['brand'], row['item_group_id'], self.model_id, file_name,
                                       str(mean_squared_error(y_test, y_pred)), mean_absolute_error(y_test, y_pred))
            
        except BaseException as e:
            log.error(module, e.__str__())
        
        log.debug(module, f"End.")
        
    def predict(self, data):
        module = self.prog+'predict()'
        
        log.debug(module, f"Start.\n{data}")
        
        #data: country, article, color, size
        
        
        try:
            model_file = open('./data/out/model/rf.pkl', 'rb')     
            regressor = pickle.load(model_file)
            
            df = pd.read_csv('./data/in/data/'+cf.latest_input_file, dtype=str)
            
            df_origin = df[(df['country']=='SINGAPORE')& (df['item_group_id'].isin(['01']))]
            
            tmp_df = df_origin.copy()
            target_attribute = tmp_df['net_soldqty']
            tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
            
            X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
            print(tmp_df.shape[0], X_train.shape[0])
            
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            
            lbl = preprocessing.LabelEncoder()
            for c in X_train.columns:
                print(c)
                if c != 'avg_moving_price':
                    X_train[c] = lbl.fit_transform(X_train[c].astype(str)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(str)) 
                else:
                    print(f"set {c} to float.")
                    X_train[c] = lbl.fit_transform(X_train[c].astype(float)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(float)) 
                    
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            # test
            y_pred = regressor.predict(X_test)
            y_pred = y_pred.round()
            log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
            log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
        except BaseException as e:
            pass

class ROSXGBoost(base_models.XGBoost):

    '''
    ROSXGBoost
    '''
    
    def __init__(self, data_file):
        
        self.prog = 'ROSXGBoost.'
        
        module = self.prog+'__init__()'
        
        super().__init__()
        
        log.debug(module, 'Start.')
        
        self.data_file = data_file
        

    def train(self):
        module = self.prog+'train()'
        
        log.debug(module, f"file: {self.data_file}")
        
        rc = cf.rc_ok
        
        try:
        
            df = pd.read_csv('./data/in/data/'+self.data_file, dtype=str)
            
            df_group = df.copy()
            df_group = df_group[['country','brand','item_group_id']]
            df_group = df_group.drop_duplicates()
            df_group = df_group[df['item_group_id'].isin(['01','02'])]
            df_group.to_csv('./log/mzz.csv', index=False, header=True)
            
            for index, row in df_group.iterrows():
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}")
            
                df_origin = df[(df['country']==row['country'])&(df['brand']==row['brand'])&(df['item_group_id'].isin([row['item_group_id']]))]
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                
                tmp_df = df_origin.copy()
                target_attribute = tmp_df['net_soldqty']
                tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
                
                X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
                print(tmp_df.shape[0], X_train.shape[0])
                
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                lbl = preprocessing.LabelEncoder()
                for c in X_train.columns:
                    print(c)
                    if c != 'avg_moving_price':
                        X_train[c] = lbl.fit_transform(X_train[c].astype(str)) 
                        X_test[c] = lbl.fit_transform(X_test[c].astype(str)) 
                    else:
                        print(f"set {c} to float.")
                        X_train[c] = lbl.fit_transform(X_train[c].astype(float)) 
                        X_test[c] = lbl.fit_transform(X_test[c].astype(float)) 
                        
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                # Create the parameter grid based on the results of random search 
                # Number of trees in random forest
                n_estimators = [x for x in range(100, 1000, 100)]
                # Number of features to consider at every split

                # Maximum number of levels in tree
                max_depth = [x for x in range(3,10)]

                # Create the random grid
                param_grid = {'n_estimators': n_estimators,
                               'max_depth': max_depth,
                               'learning_rate': [0.01,0.05, 0.1, 0.2, 0.3]}
                
                log.debug(module, f"grid search parameters: \n{param_grid}")
         
                # tuning model
                #regressor = GridSearchCV(estimator = RandomForestRegressor(), param_grid = param_grid, cv = 5, n_jobs = -1, verbose = 2)
                regressor = RandomizedSearchCV(estimator = xgb.XGBRegressor(seed = 20), 
                                               param_distributions = param_grid, 
                                               n_iter = 10,
                                               random_state = 42,
                                               return_train_score = True,
                                               n_jobs = -1, 
                                               verbose = 2)
                regressor.fit(X_train, y_train)
                log.debug(module, f"after tuning, parameters: \n{regressor.best_params_}")
                
                # test
                y_pred = regressor.predict(X_test)
                y_pred = y_pred.round()
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
                log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
                #save the model: '{model_id}_{country}_{brand}_{item_group}_{datetime}.pkl'
                dt = util.get_datetime('%Y%m%d%H%M%S', 0)
                file_name = cf.model_file.replace('{model_id}', self.model_id).replace('{country}', row['country']).replace('{brand}', row['brand']).replace('{item_group}', row['item_group_id'])
                file_name = file_name.replace('{datetime}', dt)
                model_file = open('./data/out/model/'+file_name, 'ab')
                log.debug(module, f"saved model file: {file_name}")
                #rf_SINGAPORE_CK_01_YYMMDD_v1.0.pkl
                #rf_SINGAPORE_CK_01_YYMMDD_v2.0.pkl
                pickle.dump(regressor, model_file)                     
                model_file.close()
                
                #save the model test results
                self.save_model_result(row['country'], row['brand'], row['item_group_id'], self.model_id, file_name,
                                       str(mean_squared_error(y_test, y_pred)), mean_absolute_error(y_test, y_pred))
            
        except BaseException as e:
            log.error(module, e.__str__())
            rc = cf.rc_error
        
        log.debug(module, f"End.")
        
        return rc
        
    def predict(self, data):
        module = self.prog+'predict()'
        
        log.debug(module, f"Start.\n{data}")
        
        #data: country, article, color, size
        
        
        try:
            model_file = open('./data/out/model/rf.pkl', 'rb')     
            regressor = pickle.load(model_file)
            
            df = pd.read_csv('./data/in/data/'+cf.latest_input_file, dtype=str)
            
            df_origin = df[(df['country']=='SINGAPORE')& (df['item_group_id'].isin(['01']))]
            
            tmp_df = df_origin.copy()
            target_attribute = tmp_df['net_soldqty']
            tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
            
            X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
            print(tmp_df.shape[0], X_train.shape[0])
            
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            
            lbl = preprocessing.LabelEncoder()
            for c in X_train.columns:
                print(c)
                if c != 'avg_moving_price':
                    X_train[c] = lbl.fit_transform(X_train[c].astype(str)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(str)) 
                else:
                    print(f"set {c} to float.")
                    X_train[c] = lbl.fit_transform(X_train[c].astype(float)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(float)) 
                    
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            # test
            y_pred = regressor.predict(X_test)
            y_pred = y_pred.round()
            log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
            log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
        except BaseException as e:
            pass

class ROSNeuralNet(base_models.NN):

    '''
    Neural Net
    '''
    
    def __init__(self, data_file):
        
        self.prog = 'ROSNeuralNet.'
        
        module = self.prog+'__init__()'
        
        super().__init__()
        
        log.debug(module, 'Start.')
        
        self.data_file = data_file
        

    class RegressionHyperModel(kt.HyperModel):
        def __init__(self, input_shape):
            self.input_shape = input_shape
            
        def build(self, hp):
            model = keras.Sequential()
            model.add(
                layers.Dense(
                    units=hp.Int('units', min_value=8, max_value=128, step=8),
                    activation=hp.Choice(
                        'dense_activation',
                        values=['relu'],
                        default='relu'),
                    input_shape=self.input_shape
                )
            )
            
            model.add(
                layers.Dense(
                    units=hp.Int('units', min_value=8, max_value=128, step=8),
                    activation=hp.Choice(
                        'dense_activation',
                        values=['relu'],
                        default='relu')
                )
            )
            
            model.add(
                layers.Dropout(
                    hp.Float(
                        'dropout',
                        min_value=0.0,
                        max_value=0.1,
                        default=0.005,
                        step=0.01)
                )
            )
            
            model.add(layers.Dense(1))
            
            model.compile(
                optimizer='adam', loss='mse', metrics=['mae']
            )
            
            return model

    def train(self):
        module = self.prog+'train()'
        
        log.debug(module, f"file: {self.data_file}")
        
        rc = cf.rc_ok
        
        try:
        
            df = pd.read_csv('./data/in/data/'+self.data_file, dtype=str)
            
            df_group = df.copy()
            df_group = df_group[['country','brand','item_group_id']]
            df_group = df_group.drop_duplicates()
            df_group = df_group[df['item_group_id'].isin(['01','02'])]
            df_group.to_csv('./log/mzz.csv', index=False, header=True)
            
            for index, row in df_group.iterrows():
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}")
            
                df_origin = df[(df['country']==row['country'])&(df['brand']==row['brand'])&(df['item_group_id'].isin([row['item_group_id']]))]
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                
                tmp_df = df_origin.copy()
                target_attribute = tmp_df['net_soldqty']
                tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)

                tmp_df = pd.get_dummies(tmp_df, columns=['brand', 'shopno', 'store_category', 'color', 'size', 'launch_year',
                                                        'launch_month', 'launch_week', 'batch', 'item_group_id',
                                                        'item_class_name_id', 'item_sub_class_id', 'item_department_id',
                                                        'item_category_id', 'item_product_hierarchy_id', 'item_season_id',
                                                        'item_type_id', 'item_heel_rang_id', 'launch_id', 'brand_id',
                                                        'label_id', 'sku_main_mat'], prefix='', prefix_sep='')
                tmp_df[['avg_moving_price']] = tmp_df[['avg_moving_price']].apply(pd.to_numeric) 
                
                X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
                print(tmp_df.shape[0], X_train.shape[0])
                
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                log.debug(module, f"train for country[1]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                                
                input_shape = (X_train.shape[1],)
                log.debug(module, f"train for country[2]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                hypermodel = self.RegressionHyperModel(input_shape)
                log.debug(module, f"train for country[3]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                tuner_rs = kt.RandomSearch(
                            hypermodel,
                            objective='mae',
                            seed=42,
                            max_trials=10,
                            executions_per_trial=2,
                            overwrite = True)
                log.debug(module, f"train for country[4]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                tuner_rs.search(X_train, y_train, epochs=10, validation_split=0.2, verbose=0)
                log.debug(module, f"train for country[5]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                
                dnn_model = tuner_rs.get_best_models(num_models=1)[0]
                log.debug(module, f"train for country[6]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                loss, mse = dnn_model.evaluate(X_test, y_test)
                log.debug(module, f"train for country[7]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")

                #dnn_model.evaluate(X_test, y_test)

                y_pred = dnn_model.predict(X_test)
                log.debug(module, f"train for country[8]: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                y_pred_round = y_pred.round()
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred_round)}, root: {np.sqrt(mean_squared_error(y_test, y_pred_round))}")
                log.debug(module, f"mean absolute error: {mean_absolute_error(y_test, y_pred)}")
                log.debug(module, f"mean absolute error: {mean_absolute_error(y_test, y_pred_round)}")
                

                #save the model: '{model_id}_{country}_{brand}_{item_group}_{datetime}.pkl'
                dt = util.get_datetime('%Y%m%d%H%M%S', 0)
                file_name = cf.model_file.replace('{model_id}', self.model_id).replace('{country}', row['country']).replace('{brand}', row['brand']).replace('{item_group}', row['item_group_id'])
                file_name = file_name.replace('{datetime}', dt)
                model_file = open('./data/out/model/'+file_name, 'ab')
                log.debug(module, f"saved model file: {file_name}")
                #rf_SINGAPORE_CK_01_YYMMDD_v1.0.pkl
                #rf_SINGAPORE_CK_01_YYMMDD_v2.0.pkl
                pickle.dump(dnn_model, model_file)                     
                model_file.close()
                
                #save the model test results
                self.save_model_result(row['country'], row['brand'], row['item_group_id'], self.model_id, file_name,
                                       str(mean_squared_error(y_test, y_pred)), mean_absolute_error(y_test, y_pred))
                            
        except BaseException as e:
            log.error(module, e.__str__())
            
            rc = cf.rc_error
            
        log.debug(module, f"End.")
        
        return rc
        
    

    def train_bk(self):
        module = self.prog+'train()'
        
        log.debug(module, f"file: {self.data_file}")
        
        rc = cf.rc_ok
        
        try:
        
            df = pd.read_csv('./data/in/data/'+self.data_file, dtype=str)
            
            df_group = df.copy()
            df_group = df_group[['country','brand','item_group_id']]
            df_group = df_group.drop_duplicates()
            df_group = df_group[df['item_group_id'].isin(['01','02'])]
            df_group.to_csv('./log/mzz.csv', index=False, header=True)
            
            for index, row in df_group.iterrows():
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}")
            
                df_origin = df[(df['country']==row['country'])&(df['brand']==row['brand'])&(df['item_group_id'].isin([row['item_group_id']]))]
                log.debug(module, f"train for country: {row['country']}, brand: {row['brand']}, item group: {row['item_group_id']}, no. of records: {df_origin.shape[0]}")
                
                tmp_df = df_origin.copy()
                target_attribute = tmp_df['net_soldqty']
                tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
                

                tmp_df = pd.get_dummies(tmp_df, columns=['brand', 'shopno', 'store_category', 'color', 'size', 'launch_year',
                                                        'launch_month', 'launch_week', 'batch', 'item_group_id',
                                                        'item_class_name_id', 'item_sub_class_id', 'item_department_id',
                                                        'item_category_id', 'item_product_hierarchy_id', 'item_season_id',
                                                        'item_type_id', 'item_heel_rang_id', 'launch_id', 'brand_id',
                                                        'label_id', 'sku_main_mat'], prefix='', prefix_sep='')
                tmp_df[['avg_moving_price']] = tmp_df[['avg_moving_price']].apply(pd.to_numeric) 
                
                X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
                print(tmp_df.shape[0], X_train.shape[0])
                
                y_train = y_train.astype(int)
                y_test = y_test.astype(int)
                
                def build_and_compile_model_new(x):
                    log.debug(module, f"x: {x.shape[1]}")
                    
                    model = keras.Sequential([
                      layers.Dense(128, activation='relu', input_dim=x.shape[1]),
                      layers.Dense(128, activation='relu'),
                      layers.Dense(64, activation='relu'),
                      layers.Dense(64, activation='relu'),
                      layers.Dense(32, activation='relu'),
                      layers.Dense(32, activation='relu'),
                      layers.Dense(1)])
                
                    model.compile(loss = tf.keras.losses.mse,
                                optimizer = tf.keras.optimizers.Adam(),
                                metrics = ['mae'])
                    return model   
                
                dnn_model = build_and_compile_model_new(X_train)
                log.debug(module, f"module summary: \n{dnn_model.summary()}")
            
                callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5)
                
                history = dnn_model.fit(
                    X_train,
                    y_train,
                    epochs=500, 
                    validation_split=0.20, 
                    verbose=2,
                    callbacks=[callback])
                
                log.debug(module, f"training history: \n{history}")
                
                y_pred = dnn_model.predict(X_test)
                y_pred_round = y_pred.round()
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
                log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred_round)}, root: {np.sqrt(mean_squared_error(y_test, y_pred_round))}")
                log.debug(module, f"mean absolute error: {mean_absolute_error(y_test, y_pred)}")
                log.debug(module, f"mean absolute error: {mean_absolute_error(y_test, y_pred_round)}")
                

                #save the model: '{model_id}_{country}_{brand}_{item_group}_{datetime}.pkl'
                dt = util.get_datetime('%Y%m%d%H%M%S', 0)
                file_name = cf.model_file.replace('{model_id}', self.model_id).replace('{country}', row['country']).replace('{brand}', row['brand']).replace('{item_group}', row['item_group_id'])
                file_name = file_name.replace('{datetime}', dt)
                model_file = open('./data/out/model/'+file_name, 'ab')
                log.debug(module, f"saved model file: {file_name}")
                #rf_SINGAPORE_CK_01_YYMMDD_v1.0.pkl
                #rf_SINGAPORE_CK_01_YYMMDD_v2.0.pkl
                pickle.dump(dnn_model, model_file)                     
                model_file.close()
                
                #save the model test results
                self.save_model_result(row['country'], row['brand'], row['item_group_id'], self.model_id, file_name,
                                       str(mean_squared_error(y_test, y_pred)), mean_absolute_error(y_test, y_pred))
                            
        except BaseException as e:
            log.error(module, e.__str__())
            
            rc = cf.rc_error
            
        log.debug(module, f"End.")
        
        return rc
        
    
    
    def predict(self, data):
        module = self.prog+'predict()'
        
        log.debug(module, f"Start.\n{data}")
        
        #data: country, article, color, size
        
        
        try:
            model_file = open('./data/out/model/rf.pkl', 'rb')     
            regressor = pickle.load(model_file)
            
            df = pd.read_csv('./data/in/data/'+cf.latest_input_file, dtype=str)
            
            df_origin = df[(df['country']=='SINGAPORE')& (df['item_group_id'].isin(['01']))]
            
            tmp_df = df_origin.copy()
            target_attribute = tmp_df['net_soldqty']
            tmp_df = tmp_df.drop(columns=['net_soldqty','country'], axis=1)
            
            X_train, X_test, y_train, y_test = train_test_split(tmp_df, target_attribute, test_size=0.2, random_state=42)
            print(tmp_df.shape[0], X_train.shape[0])
            
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            
            lbl = preprocessing.LabelEncoder()
            for c in X_train.columns:
                print(c)
                if c != 'avg_moving_price':
                    X_train[c] = lbl.fit_transform(X_train[c].astype(str)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(str)) 
                else:
                    print(f"set {c} to float.")
                    X_train[c] = lbl.fit_transform(X_train[c].astype(float)) 
                    X_test[c] = lbl.fit_transform(X_test[c].astype(float)) 
                    
            y_train = y_train.astype(int)
            y_test = y_test.astype(int)
            # test
            y_pred = regressor.predict(X_test)
            y_pred = y_pred.round()
            log.debug(module, f"mean squared error: {mean_squared_error(y_test, y_pred)}, root: {np.sqrt(mean_squared_error(y_test, y_pred))}")
            log.debug(module, f"mean abosolute error: {mean_absolute_error(y_test, y_pred)}")
    
        except BaseException as e:
            pass
        
        
    