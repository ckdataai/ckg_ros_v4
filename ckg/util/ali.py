import psycopg2
from datetime import datetime
import pandas as pd
import numpy as np
import pandas.io.sql as sqlio
import sys, os
from os.path import abspath

import ckg.conf.config as cf
import ckg.util.log as log
import ckg.util.util as util


prog = 'ali.'

def connect_to_ali():
    module = prog+'connect_to_ali()'
    
    con = None
    try:
        con = psycopg2.connect(
            host     = cf.ali_host,
            port     = cf.ali_port,
            database = cf.ali_database_dw, #ali_database_oms
            user     = cf.ali_user,
            password = cf.ali_password)
        
    except BaseException as e:
        log.error(module, e.__str__())
        con = None
        
    return con

def connect_to_ali_ds():
    module = prog+'connect_to_ali_ds()'
    
    con = None
    try:
        con = psycopg2.connect(
            host     = cf.ali_host,
            port     = cf.ali_port,
            database = cf.ali_database_oms,
            user     = cf.ali_user,
            password = cf.ali_password)
        
    except BaseException as e:
        log.error(module, e.__str__())
        con = None
        
    return con


def connect_to_ali_gwh():
    module = prog+'connect_to_ali_gwh()'
    
    con = None
    try:
        con = psycopg2.connect(
            host     = cf.ali_host,
            port     = cf.ali_port,
            database = cf.ali_database_gwh,
            user     = cf.ali_user,
            password = cf.ali_password)
        
    except BaseException as e:
        log.error(module, e.__str__())
        con = None
        
    return con

def execute(sql):
    module = prog+'execute()'
    
    #log.debug(module, sql)
    
    rc = cf.rc_ok
    
    try:
                
        con = connect_to_ali()
        if con == None:
            cf.rc_error
        cur = con.cursor()
        cur.execute(sql)
        cur.close()
                
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_soh():
    module = prog+'get_soh()'
    
    rc = cf.rc_ok
    
    try:
        # sql = 'select distinct t1.level1_code, t2.launch_id, t1.article_number, t1.sap_barcode, t2.batch, t2.color, t2.size, t1.colour_code, t1.sizes_code from gwh_sales t1, gwh_items t2' + \
        #         ' where t1.country_code=\''+country+'\'' + \
        #         '   and t1.brand=\'CK\' and t1.level1_code in (\'01\',\'02\') ' + \
        #         '   and to_date(t1.sales_date, \'YYYY-MM-DD\')>= now() - interval \''+str(weeks)+' week\'' + \
        #         '   and t1.sap_barcode = t2.barcode ' + \
        #         '   and t1.sales_qty > 0 ' + \
        #         '  order by level1_code, article_number, colour_code, sizes_code'

                
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        dt = util.get_datetime('%Y%m%d%H%M%S', 0)
        sql = f'''
                select * from ds_dwd_soh_daily
            '''                    
        df = sqlio.read_sql_query(sql, con)
        df.to_csv('./log/sql_soh.csv', index=False, header=True)
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_soh_2():
    module = prog+'get_soh_2()'
    
    rc = cf.rc_ok
    
    try:
        # sql = 'select distinct t1.level1_code, t2.launch_id, t1.article_number, t1.sap_barcode, t2.batch, t2.color, t2.size, t1.colour_code, t1.sizes_code from gwh_sales t1, gwh_items t2' + \
        #         ' where t1.country_code=\''+country+'\'' + \
        #         '   and t1.brand=\'CK\' and t1.level1_code in (\'01\',\'02\') ' + \
        #         '   and to_date(t1.sales_date, \'YYYY-MM-DD\')>= now() - interval \''+str(weeks)+' week\'' + \
        #         '   and t1.sap_barcode = t2.barcode ' + \
        #         '   and t1.sales_qty > 0 ' + \
        #         '  order by level1_code, article_number, colour_code, sizes_code'

                
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        dt = util.get_datetime('%Y%m%d%H%M%S', 0)
        sql = f'''
                SELECT distinct country, substring(date, 1,7) as month FROM public.ds_dwd_soh_daily order by country, month
            '''                    
        df = sqlio.read_sql_query(sql, con)
        i = 0
        for index, row in df.iterrows():
            country = row['country']
            month = row['month']
            log.debug(module, f"retrieving for {i+1}, {row['country']}, {row['month']}")
            
            sql = f'''
                    select * from ds_dwd_soh_daily where country='{country}' and substr(date,1,7)='{month}' order by country, date, articlenumber 
                '''
            df_data = sqlio.read_sql_query(sql, con)
            if i == 0:
                
                df_data.to_csv('./log/total_soh.csv', index=False, header=True)
            else:
                df_data.to_csv('./log/total_soh.csv', mode='a', index=False, header=False)
                
            i = i + 1
        
        con.close()
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc




def get_trnnum():
    module = prog+'get_trnnum()'
    
    log.debug(module, 'Start.')
    
    trnnum = cf.rc_error
    
    cur = None
    con = None
    try:
        t = 11

        #log.debug(module, sql)
        
        #rc = execute(sql)
        con = connect_to_ali()
        if con == None:
            cf.rc_error
            
        cur = con.cursor()
        
        sql = f'''
                select trnnum from sdrf where recnum = 1;
            '''

        df = sqlio.read_sql_query(sql, con)   
        trnnum = df.iloc[0,0]

        sql = f'''
                update sdrf set trnnum = trnnum + 1;
            '''
        cur.execute(sql)
        
    except BaseException as e:
        log.error(module, e.__str__())

        trnnum = cf.rc_error
    finally:
        con.close()
        
    return str(trnnum).zfill(9)

def get_objnum():
    module = prog+'get_objnum()'
    
    log.debug(module, 'Start.')
    
    objnum = cf.rc_error
    
    cur = None
    con = None
    try:
        t = 11

        #log.debug(module, sql)
        
        #rc = execute(sql)
        con = connect_to_ali()
        if con == None:
            cf.rc_error
            
        cur = con.cursor()
        
        sql = f'''
                select objnum from sdrf where recnum = 1;
            '''

        df = sqlio.read_sql_query(sql, con)   
        objnum = df.iloc[0,0]

        sql = f'''
                update sdrf set objnum = objnum + 1;
            '''
        cur.execute(sql)
        
    except BaseException as e:
        log.error(module, e.__str__())

        trnnum = cf.rc_error
    finally:
        con.close()
        
    return str(objnum).zfill(9)

def get_objnum_2(con, n, return_objnum):
    module = prog+'get_objnum_2()'
    
    log.debug(module, f'Start, n: {n}')
    
    rc = cf.rc_ok
    
    objnum = 0
    
    cur = None
    try:
            
        cur = con.cursor()
        
        sql = f'''
                select objnum from sdrf where recnum = 1;
            '''

        df = sqlio.read_sql_query(sql, con)   
        objnum = df.iloc[0,0]

        sql = f'''
                update sdrf set objnum = objnum + {n};
            '''
        cur.execute(sql)
        
    except BaseException as e:
        rc = cf.rc_error
        log.error(module, e.__str__())

    if rc == cf.rc_ok and objnum>0:
        #return_objnum[0] = str(objnum).zfill(9)
        return_objnum[0] = objnum
    
        
    return rc


def upload_soh_if(uc, df):
    module = prog+'upload_soh_if()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_error
    
    cur = None
    con = None
    try:
        trnnum = get_trnnum()
        log.debug(module, f'new trnnum: {trnnum}')

        #log.debug(module, sql)
        
        #rc = execute(sql)
        con = connect_to_ali()
        if con == None:
            cf.rc_error
            
        cur = con.cursor()
        

        # sql = f'''
        #         delete from if_soh;
        #         insert into if_soh values(1, 'A1', 'B1', 2, {trnnum});
        #         insert into if_soh values(2, 'A1', 'B1', 2, {trnnum});
        #         insert into if_soh values(3, 'A1', 'B1', 2, {trnnum});
        #         insert into if_soh values(4, 'A1', 'B1', 2, {trnnum});
        #         insert into if_soh values(5, 'A1', 'B1', 2, {trnnum});
        #     '''
            
        error_list = []
        seq = 1
        sql =  f'delete from if_soh;' 
        for index, row in df.iterrows():
            sql = sql + '\n' + f"insert into if_soh (seq, article, batch, qty, trnnum, lsdt_objnum, loc_id) values({seq}, '{row['Article']}', '{row['Batch']}', {row['Qty']}, '{trnnum}', '0', 'CN_DC');" 
            seq = seq + 1

        #print(sql)
        cur.execute(sql)
        log.debug(module, 'step1.')

        sql = f'''
                update if_soh t1 set lsdt_objnum= t2.objnum from lsdt t2 where t1.article = t2.article and t1.batch = t2.batch and t1.loc_id=t2.loc_id; 
                '''
        cur.execute(sql)
        log.debug(module, 'step2.')


        # not initial, still open: not sent yet, or some filtering criteria
        sql = f'''
                update lsdt t1 set status=10, avlqty = t2.qty, trnnum=t2.trnnum, upddat=current_timestamp, updusr=user from if_soh t2 where t1.objnum = t2.lsdt_objnum and t2.qty>0 and t1.status='10' and t1.open_qty=0;
                '''
        cur.execute(sql)
        log.debug(module, 'step3.')

         
        # not updated yet, still open: not sent yet, or some filtering criteria
        sql = f'''
                update lsdt t1 set status=10, avlqty = t2.qty, trnnum=t2.trnnum, upddat=current_timestamp, updusr=user from if_soh t2 where t1.objnum = t2.lsdt_objnum  and t2.qty>0 and t1.status='10' and t1.open_qty>0;
                '''
        cur.execute(sql)
        log.debug(module, 'step4.')

        # initial or update before
        sql = f'''
                update lsdt t1 set status=10, open_qty=avlqty, avlqty = t2.qty, trnnum=t2.trnnum, upddat=current_timestamp, updusr=user from if_soh t2 where t1.objnum = t2.lsdt_objnum  and t2.qty>0 and t1.status='90' and t1.avlqty>0;
                '''
        cur.execute(sql)
        log.debug(module, 'step5.')

        # terminated before, then now update again
        sql = f'''
                update lsdt t1 set status=10, avlqty = t2.qty, trnnum=t2.trnnum, upddat=current_timestamp, updusr=user from if_soh t2 where t1.objnum = t2.lsdt_objnum  and t2.qty>0 and t1.status='90' and t1.open_qty>0 and t1.avlqty=0;
                '''
        cur.execute(sql)
        log.debug(module, 'step6.')

        # terminated 
        sql = f'''
                update lsdt t1 set status=10, open_qty=avlqty, avlqty = t2.qty, trnnum=t2.trnnum, upddat=current_timestamp, updusr=user from if_soh t2 where t1.objnum = t2.lsdt_objnum  and t2.qty=0 and t1.status='90';
                '''
        cur.execute(sql)
        log.debug(module, 'step7.')


        sql = f'''
                select article, batch, qty,trnnum from if_soh where lsdt_objnum = '0';  
                '''
                
        # df_new = sqlio.read_sql_query(sql, con)
        # for index, row in df_new.iterrows():
        #     objnum = get_objnum()
        #     sql = sql + '\n' + f"insert into lsdt(objnum, status, article, batch, avlqty, in_qty, out_qty, open_qty, loc_id, crtdat, upddat, crtusr, trnnum, updusr) values('{objnum}', 10, '{row['article']}', '{row['batch']}', {row['qty']}, 0, 0, 0, 'CN_DC', current_timestamp , current_timestamp, user, '{row['trnnum']}', user);" 
        # cur.execute(sql)
        
        df_new = sqlio.read_sql_query(sql, con)
        i = 0
        new_sql = ''
        #str(objnum).zfill(9)
        objnum = 0
        for index, row in df_new.iterrows():
            if objnum == 0:
                objnum_return = [0]
                rc = get_objnum_2(con, df_new.shape[0], objnum_return)
                if rc != cf.rc_ok:
                    log.error(module, 'call get_objnum_2() failed.')
                    return cf.rc_error
                objnum = objnum_return[0]
                
            objnum_s = str(objnum).zfill(9)
            # if i == 0:
            #     new_sql = f"insert into lsdt(objnum, status, article, batch, avlqty, in_qty, out_qty, open_qty, loc_id, crtdat, upddat, crtusr, trnnum, updusr) values('{objnum}', 10, '{row['article']}', '{row['batch']}', {row['qty']}, 0, 0, 0, 'CN_DC', current_timestamp , current_timestamp, user, '{row['trnnum']}', user);" 
            # else:
            #     new_sql = new_sql + '\n' + f"insert into lsdt(objnum, status, article, batch, avlqty, in_qty, out_qty, open_qty, loc_id, crtdat, upddat, crtusr, trnnum, updusr) values('{objnum}', 10, '{row['article']}', '{row['batch']}', {row['qty']}, 0, 0, 0, 'CN_DC', current_timestamp , current_timestamp, user, '{row['trnnum']}', user);" 

            if i == 0:
                new_sql = f"insert into lsdt(objnum, status, article, batch, avlqty, in_qty, out_qty, open_qty, loc_id, crtdat, upddat, crtusr, trnnum, updusr) values('{objnum_s}', 10, '{row['article']}', '{row['batch']}', {row['qty']}, 0, 0, 0, 'CN_DC', current_timestamp , current_timestamp, user, '{row['trnnum']}', user)" 
            else:
                new_sql = new_sql + ',' + f"('{objnum_s}', 10, '{row['article']}', '{row['batch']}', {row['qty']}, 0, 0, 0, 'CN_DC', current_timestamp , current_timestamp, user, '{row['trnnum']}', user)" 
            i = i + 1
            if i == 3000:
                new_sql = new_sql + ';'
                cur.execute(new_sql)
                i = 0
                new_sql = ''
            
            objnum = objnum + 1
            
        if i != 0:
            new_sql = new_sql + ';'
            cur.execute(new_sql)
            
        log.debug(module, 'step8.')
                
        
    except BaseException as e:
        log.error(module, e.__str__())

        rc = cf.rc_error
    finally:
        con.close()
        
    return rc


def get_sku_data(uc, return_df):
    module = prog+'get_sku_data()'
    
    rc = cf.rc_ok
    
    try:
        # sql = 'select distinct t1.level1_code, t2.launch_id, t1.article_number, t1.sap_barcode, t2.batch, t2.color, t2.size, t1.colour_code, t1.sizes_code from gwh_sales t1, gwh_items t2' + \
        #         ' where t1.country_code=\''+country+'\'' + \
        #         '   and t1.brand=\'CK\' and t1.level1_code in (\'01\',\'02\') ' + \
        #         '   and to_date(t1.sales_date, \'YYYY-MM-DD\')>= now() - interval \''+str(weeks)+' week\'' + \
        #         '   and t1.sap_barcode = t2.barcode ' + \
        #         '   and t1.sales_qty > 0 ' + \
        #         '  order by level1_code, article_number, colour_code, sizes_code'

                
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        dt = util.get_datetime('%Y%m%d%H%M%S', 0)
        sql = f'''
                select objnum, loc_id, article, batch, avlqty from lsdt where status='10' and open_qty=0 and avlqty>0 order by article, batch;
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df['initial'] = df
        df.to_csv('./log/01_sku_list_initial_'+dt+'.csv', index=False, header=True)
        
        sql = f'''
                select objnum, loc_id, article, batch, avlqty from lsdt where status='10' and open_qty>0 and avlqty>0 order by article, batch;
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df['update'] = df
        df.to_csv('./log/02_sku_list_update_'+dt+'.csv', index=False, header=True)

        sql = f'''
                select objnum, loc_id, article, batch, avlqty from lsdt where status='10' and open_qty>0 and avlqty=0 order by article, batch;
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df['remove'] = df
        df.to_csv('./log/03_sku_list_remove_'+dt+'.csv', index=False, header=True)

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_sku_data_first_import(uc, return_df):
    module = prog+'get_sku_data_first_import()'
    
    rc = cf.rc_ok
    
    try:
        # sql = 'select distinct t1.level1_code, t2.launch_id, t1.article_number, t1.sap_barcode, t2.batch, t2.color, t2.size, t1.colour_code, t1.sizes_code from gwh_sales t1, gwh_items t2' + \
        #         ' where t1.country_code=\''+country+'\'' + \
        #         '   and t1.brand=\'CK\' and t1.level1_code in (\'01\',\'02\') ' + \
        #         '   and to_date(t1.sales_date, \'YYYY-MM-DD\')>= now() - interval \''+str(weeks)+' week\'' + \
        #         '   and t1.sap_barcode = t2.barcode ' + \
        #         '   and t1.sales_qty > 0 ' + \
        #         '  order by level1_code, article_number, colour_code, sizes_code'

                
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        dt = util.get_datetime('%Y%m%d%H%M%S', 0)
        sql = f'''
                select objnum, loc_id, article, batch, avlqty from lsdt where status='10' and open_qty=0 and avlqty>0 order by article, batch;
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df['initial'] = df
        df.to_csv('./log/01a_sku_list_initial_'+dt+'.csv', index=False, header=True)
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_latest_soh_list(uc, return_df):
    module = prog+'get_latest_soh_list()'
    
    rc = cf.rc_ok
    
    try:
        # sql = 'select distinct t1.level1_code, t2.launch_id, t1.article_number, t1.sap_barcode, t2.batch, t2.color, t2.size, t1.colour_code, t1.sizes_code from gwh_sales t1, gwh_items t2' + \
        #         ' where t1.country_code=\''+country+'\'' + \
        #         '   and t1.brand=\'CK\' and t1.level1_code in (\'01\',\'02\') ' + \
        #         '   and to_date(t1.sales_date, \'YYYY-MM-DD\')>= now() - interval \''+str(weeks)+' week\'' + \
        #         '   and t1.sap_barcode = t2.barcode ' + \
        #         '   and t1.sales_qty > 0 ' + \
        #         '  order by level1_code, article_number, colour_code, sizes_code'

                
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        dt = util.get_datetime('%Y%m%d%H%M%S', 0)
        sql = f'''
                select * from (
                select objnum, 'Active-1st Import' status, loc_id, article, batch, open_qty, avlqty from lsdt
                where status='90' and avlqty>0 and open_qty=0
                union ALL 
                select objnum, 'Active-Topup' status, loc_id, article, batch, open_qty, avlqty from lsdt
                where status='90' and avlqty>0 and open_qty>0
                union all
                select objnum, 'Inactive' status, loc_id, article, batch, open_qty, avlqty from lsdt
                where status='10' and open_qty=0 and avlqty>0
                union ALL 
                select objnum, 'Removed' status, loc_id, article, batch, open_qty, avlqty from lsdt
                where status='90' and avlqty=0 and open_qty>0
                ) t 
                order by status

            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df['soh'] = df
        df.to_csv('./log/20_soh_list_'+dt+'.csv', index=False, header=True)
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_active_soh_list(uc, return_df):
    module = prog+'get_active_soh_list()'
    
    rc = cf.rc_ok
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        dt = util.get_datetime('%Y%m%d%H%M%S', 0)
        sql = f'''
                select objnum, loc_id, article, batch, open_qty, avlqty from lsdt
                where status='90' and avlqty>0 
                order by article, batch
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df['soh'] = df
        df.to_csv('./log/21_soh_list_'+dt+'.csv', index=False, header=True)
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def complete_processing(uc, complete_ignore_list):
    module = prog+'complete_processing()'
    
    log.debug(module, 'Start, complete_ignore_list.length['+str(len(complete_ignore_list))+']')
    
    rc = cf.rc_ok
    
    cur = None
    con = None
    try:
        trnnum = get_trnnum()
        log.debug(module, f'new trnnum: {trnnum}')

        #log.debug(module, sql)
        
        #rc = execute(sql)
        con = connect_to_ali()
        if con == None:
            cf.rc_error
            
        cur = con.cursor()
        
        sql = ''
        ignore_list = str(tuple(complete_ignore_list))
        if len(complete_ignore_list) > 0:
            sql = f'''
                    update lsdt t1 set status='90', trnnum='{trnnum}', upddat=current_timestamp, updusr=user where status = '10'
                       and objnum not in {ignore_list};
                '''
        else:
            sql = f'''
                    update lsdt t1 set status='90', trnnum='{trnnum}', upddat=current_timestamp, updusr=user where status = '10';
                '''
        cur.execute(sql)
        log.debug(module, 'step3.')
                
        
    except BaseException as e:
        log.error(module, e.__str__())

        rc = cf.rc_error
    finally:
        con.close()
        
    return rc

def remove_soh_by_objnum(uc, removal_list):
    module = prog+'remove_soh_by_objnum()'
    
    log.debug(module, 'Start, removal_list.length['+str(len(removal_list))+']')
    
    rc = cf.rc_ok
    
    cur = None
    con = None
    try:
        trnnum = get_trnnum()
        log.debug(module, f'new trnnum: {trnnum}')

        #log.debug(module, sql)
        
        #rc = execute(sql)
        con = connect_to_ali()
        if con == None:
            cf.rc_error
            
        cur = con.cursor()
        
        sql = ''
        removal_list = str(tuple(removal_list))
        sql = f'''
                update lsdt t1 set open_qty=avlqty, avlqty=0, trnnum='{trnnum}', upddat=current_timestamp, updusr=user 
                where objnum in {removal_list};
            '''

        cur.execute(sql)
        log.debug(module, 'step3.')
                
        
    except BaseException as e:
        log.error(module, e.__str__())

        rc = cf.rc_error
    finally:
        con.close()
        
    return rc


def complete_processing_list(uc, complete_list):
    module = prog+'complete_processing_list()'
    
    log.debug(module, 'Start, complete_list.length['+str(len(complete_list))+']')
    
    rc = cf.rc_ok
    
    cur = None
    con = None
    try:
        trnnum = get_trnnum()
        log.debug(module, f'new trnnum: {trnnum}')

        #log.debug(module, sql)
        
        #rc = execute(sql)
        con = connect_to_ali()
        if con == None:
            cf.rc_error
            
        cur = con.cursor()
        
        sql = ''
        complete_list = str(tuple(complete_list))
        sql = f'''
                update lsdt t1 set status='90', trnnum='{trnnum}', upddat=current_timestamp, updusr=user where status = '10'
                   and objnum in {complete_list};
            '''

        cur.execute(sql)
        log.debug(module, 'step3.')
                
        
    except BaseException as e:
        log.error(module, e.__str__())

        rc = cf.rc_error
    finally:
        con.close()
        
    return rc



def get_single_sku_data(uc, article, batch, return_df):
    module = prog+'get_single_sku_data()'
    
    log.debug(module, f'Start, {article}, {article}')
    
    rc = cf.rc_ok
    
    try:
        con = connect_to_ali_gwh()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select * from gwh_items where article_number='{article}' and batch='{batch}';
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_single_sku_data_all(uc, return_df):
    module = prog+'get_single_sku_data()'
    
    log.debug(module, f'Start.')
    
    rc = cf.rc_ok
    
    try:
        con = connect_to_ali_gwh()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select * from gwh_items order by article_number, batch;
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_sku_soh(uc, article, batch, qty_return):
    module = prog+'get_sku_soh()'
    
    log.debug(module, f'Start {article}, {batch}.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                    select sum(t1.stock_on_hand) from ecom_soh t1 
                        where t1.article_number='{article}' and t1.batch='{batch}'
                        and t1.storage_location_id in ('1010','1100')
                        and t1.stock_on_hand_date = (select max(stock_on_hand_date) from ecom_soh)
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            qty = 0
        else:
            qty = df.iloc[0][0]
            
        qty_return[0] = qty

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_sku_soh_all(uc, return_df):
    module = prog+'get_sku_soh_all()'
    
    log.debug(module, f'Start.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                    select article_number, batch, sum(t1.stock_on_hand) from ecom_soh t1 
                        where t1.storage_location_id in ('1010','1100')
                        and t1.stock_on_hand_date = (select max(stock_on_hand_date) from ecom_soh)
                        group by t1.article_number, t1.batch
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_color_soh(uc, article, color, qty_return):
    module = prog+'get_color_soh()'
    
    log.debug(module, 'Start {article}, {batch}.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                    select sum(t1.stock_on_hand) from ecom_soh t1 
                        where t1.article_number='{article}' and t1.batch like '{color}%'
                        and t1.storage_location_id in ('1010','1100')
                        and t1.stock_on_hand_date = (select max(stock_on_hand_date) from ecom_soh)
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            qty = 0
        else:
            qty = df.iloc[0][0]
            
        qty_return[0] = qty

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_color_soh_all(uc, return_df):
    module = prog+'get_color_soh_all()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                    select article_number, SPLIT_PART(batch, '_', 1) color, sum(t1.stock_on_hand) from ecom_soh t1
                    where t1.storage_location_id in ('1010','1100')
                    and t1.stock_on_hand_date = (select max(stock_on_hand_date) from ecom_soh)
                    group by t1.article_number, color
                    order by t1.article_number, color
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_article_soh(uc, article, qty_return):
    module = prog+'get_sku_soh()'
    
    log.debug(module, 'Start {article}, {batch}.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                    select sum(t1.stock_on_hand) from ecom_soh t1 
                        where t1.article_number='{article}' 
                        and t1.storage_location_id in ('1010','1100')
                        and t1.stock_on_hand_date = (select max(stock_on_hand_date) from ecom_soh)
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            qty = 0
        else:
            qty = df.iloc[0][0]
            
        qty_return[0] = qty

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_article_soh_all(uc, return_df):
    module = prog+'get_article_soh_all()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                    select article_number, sum(t1.stock_on_hand) from ecom_soh t1 
                        where t1.storage_location_id in ('1010','1100')
                        and t1.stock_on_hand_date = (select max(stock_on_hand_date) from ecom_soh)
                        group by t1.article_number
                        order by t1.article_number
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_sku_sold_qty(uc, article, batch, qty_return):
    module = prog+'get_sku_sold_qty()'
    
    log.debug(module, 'Start {article}, {batch}.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select sum(ordered_quantity) from ecom_sales
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th') and article='{article}' and article_batch = '{batch}'
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            qty = 0
        else:
            qty = df.iloc[0][0]
            
        qty_return[0] = qty

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_sku_sold_qty_all(uc, return_df):
    module = prog+'get_sku_sold_qty_all()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select article_number, batch, sum(ordered_quantity) from ecom_sales
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th')
                group by article_number, batch
            '''                    
        df = sqlio.read_sql_query(sql, con)
        
        return_df[0] = df

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_color_sold_qty(uc, article, color, qty_return):
    module = prog+'get_color_sold_qty()'
    
    log.debug(module, 'Start {article}, {color}.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select sum(ordered_quantity) from ecom_sales
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th') and article='{article}' and article_batch like '{color}%'
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            qty = 0
        else:
            qty = df.iloc[0][0]
            
        qty_return[0] = qty

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

#mike here
#                    select article_number, SPLIT_PART(batch, '_', 1) color, sum(t1.stock_on_hand) from ecom_soh t1
def get_color_sold_qty_all(uc, return_df):
    module = prog+'get_color_sold_qty_all()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select article, SPLIT_PART(article_batch, '_', 1) color, sum(ordered_quantity) from ecom_sales t1
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th')
                group by t1.article, color
                order by t1.article, color
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc



def get_article_sold_qty(uc, article, qty_return):
    module = prog+'get_color_qty()'
    
    log.debug(module, 'Start {article}.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select sum(ordered_quantity) from ecom_sales
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th') and article='{article}'
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            qty = 0
        else:
            qty = df.iloc[0][0]
            
        qty_return[0] = qty

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_article_sold_qty_all(uc, return_df):
    module = prog+'get_article_sold_qty_all()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    qty = 0
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select article, sum(ordered_quantity) from ecom_sales
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th')
                group by article
                order by article
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc


def get_article_oldest_sales(uc, article, dt_return):
    module = prog+'get_color_qty()'
    
    log.debug(module, 'Start {article}.')
    
    rc = cf.rc_ok
    
    dt = ''
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select min(order_date) from ecom_sales
               where  external_site_id not in('ck-id','ck-jp','ck-kr','ck-th')
                where article='{article}'
            '''                    
        df = sqlio.read_sql_query(sql, con)
        if df.iloc[0][0] == None:
            dt = ''
        else:
            dt = df.iloc[0][0]
            
        dt_return[0] = dt

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_article_oldest_sales_all(uc, return_df):
    module = prog+'get_article_oldest_sales_all()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    dt = ''
    
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select article, min(order_date) from ecom_sales
                where external_site_id not in('ck-id','ck-jp','ck-kr','ck-th')
                group by article
                order by article
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df[0] = df


    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc

def get_stock_type(uc, article_list, return_df):
    module = prog+'get_stock_type()'
    
    log.debug(module, 'Start.')
    
    rc = cf.rc_ok
    
    dt = ''
    
    article_list_tuple = tuple(article_list)
    article_list_tuple = str(article_list_tuple)
    article_list_tuple = article_list_tuple.replace(',)', ')')
    try:
        con = connect_to_ali()
        if con == None:
            rc = cf.rc_error
        
        sql = f'''
                select t1.* from stock_type_41 t1 
                 where t1.articleno in {article_list_tuple}
            and not EXISTS (
                    select * from stock_type_41 t2 where t1.articleno=t2.articleno and t2.stocktypedate>t1.stocktypedate)
                order by articleno
            '''                    
        df = sqlio.read_sql_query(sql, con)
        return_df.append(df)

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
        
    return rc
