# -*- coding: utf-8 -*-
"""
Created on Thu Jun  3 09:28:02 2021

@author: mike.zhang
"""

import pyodbc 

def connect_sql_server(server, db, user, pwd):
    #server = 'pvt-dbsvr02' 
    #database = 'Online_ED' 
    #username = 'sf' 
    #password = 'sf123456' 
    conn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+db+';UID='+user+';PWD='+ pwd)
    
    return conn


#samples
# =============================================================================
# def connect_sql():
#     server = 'pvt-dbsvr02' 
#     database = 'Online_ED' 
#     username = 'sf' 
#     password = 'sf123456' 
#     conn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
#     #cursor = cnxn.cursor()
#     
#     df = pd.read_sql_query('SELECT * FROM Online_ED.dbo.Vw_CKMemberSalesData', conn)
# 
#     print('records['+str(len(df.index))+']')
# 
# def connect_sql_s():
#     server = 'pvt-dbsvr02' 
#     database = 'Online_ED' 
#     username = 'sf' 
#     password = 'sf123456' 
#     conn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
#     
#     sql = "{call [Online_ED].[dbo].[ProcMemberOrderExtraction_R2](?,?)}"
#     params = ('CK','')
#     cursor =  conn.cursor()
#     rows = cursor.execute(sql, params).fetchall()
#     print(rows[0])
# 
#  
#     # Close the cursor and delete it
#     cursor.close()
#     del cursor
#  
#     # Close the database connection
#     conn.close()
# 
# =============================================================================
