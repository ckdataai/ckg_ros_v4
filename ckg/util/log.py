# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 12:13:07 2021

@author: mike.zhang
"""

import sys
import os
import ckg.conf.config as cf

from datetime import datetime

def debug(module, msg):
    if cf.log_LEVEL == cf.log_DEBUG:
        log(cf.log_DEBUG, module, msg)
                
def error(module, msg):
    if cf.log_LEVEL == cf.log_DEBUG or cf.log_LEVEL == cf.log_ERROR:
        log(cf.log_ERROR, module, msg)

def log(log_level, module, msg):
    f = open(cf.log_FILE, 'a', encoding='utf-8')
    
    now = datetime.now()
    line = now.strftime('%Y.%m.%d %H:%M:%S.%f')[0:-3]
    line = line+'  '+log_level+'  '+module.ljust(20)+'  '+str(msg)+'\n'
    f.write(line)
    
    f.close()
                        

    
