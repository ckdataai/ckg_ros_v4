"""
import pyrfc
from pyrfc import *
import ibm_db, ibm_db_dbi
import pandas as pd
import sys

import ckg.conf.config as cf
import ckg.util.log as log


print('loaded['+str(pyrfc.__version__)+']')

prog = 'sap.'


def connect_to_sap():
    module = prog+'connect_to_sap()'
    
    con = None
    
    try:
        #con = Connection(ashost='192.168.1.69', sysnr='00', client='888', user='mike.zhang', passwd='M!keck123')
        #con = Connection(ashost='10.65.6.55', sysnr='00', client='200', user='mike.zhang', passwd='M!keck123')
        
        con = Connection(ashost=cf.erp_ashost, sysnr=cf.erp_sysnr, client=cf.erp_client, user=cf.erp_user, passwd=cf.erp_password)
        log.debug(module, 'connected.')
        
        
        # response = con.call(
        #     'RFC_READ_TABLE',
        #     QUERY_TABLE='MARA',
        #     DELIMITER=',',
        #     FIELDS=['MATNR','MBRSH','ERSDA'],
        #     #OPTIONS=options,
        #     ROWCOUNT=10,
        #     ROWSKIPS=0,
        # )
    
        # print(response)
    
        # response = con.call(
        #     'ZBAPI_QUERY',
        #     QUERY='SELECT T1~COLOR T1~NAME FROM  ZSKU_COLOR_SN AS T1 INTO TABLE LT_DATA'
        # )
        # print('ok')
        # print(response)
    
    except BaseException as e:
        log.error(module, e.__str__())
        con = None
        
    return con
        
def connect_to_sap_db(uc):
    module = prog+'connect_to_sap_db()'
    
    con = None
    
    try:
        con = ibm_db.connect(cf.sap_prd_db, "", "")
    except BaseException as e:
        log.error(module, e.__str__())
        con = None
    
    return con

def get_article_average_cost(uc, article_list, df_result):
    module = prog+'get_article_average_cost()'
    
    rc = cf.rc_ok
    
    try:
        con = connect_to_sap_db(uc)
        sql = f'''
                select * from SAPPRD.MBEW where BWKEY='1000' and BKLAS='3100' and matnr in {article_list}
                '''
       
        df = pd.read_sql(sql, ibm_db_dbi.Connection(con))
        
        df.to_csv('./log/mbew.csv', index=False, header=True)
        
        df_result.append(df)
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
    
    return rc


def get_currency_ex_rate(uc, fc, tc, return_rate):
    module = prog+'get_article_average_cost()'
    
    rc = cf.rc_ok
    
    try:
        con = connect_to_sap_db(uc)
        sql = f'''
                select * from SAPPRD.TCURR where KURST='M' and FCURR='{fc}' and TCURR='{tc}' order by GDATU
                '''
       
        df = pd.read_sql(sql, ibm_db_dbi.Connection(con))
        if df.shape[0]>0:
            print('rate:', df.loc[0,'UKURS'])
        df.to_csv('./log/tcurr.csv', index=False, header=True)
        
        return_rate.append(df.loc[0,'UKURS'])

    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
    
    return rc
    
def get_pending_po(uc, df_result):
    module = prog+'get_pending_po()'
    
    print('anyway.')
    rc = cf.rc_ok
    
    try:
        con = connect_to_sap_db(uc)
        sql = f'''
                select t1.EBELN, t2.MATNR, t3.CHARG,t3.VBELN, t4.WADAT_IST from SAPPRD.EKKO t1, SAPPRD.EKPO t2, SAPPRD.EKES t3, SAPPRD.LIKP t4 
                where t1.BUKRS='1000' and t1.BSART='CS'
                and t1.EBELN=t2.EBELN and t1.EBELN=t3.EBELN and t2.EBELP=t3.EBELP
                and t3.MENGE > 0
                and t3.VBELN=t4.VBELN and t4.WADAT_IST = '00000000'
                '''
        # sql = f'''
        #         select t4.VBELN, t4.WADAT_IST from SAPPRD.LIKP t4 where WADAT_IST='00000000'
        #         '''
       
        df = pd.read_sql(sql, ibm_db_dbi.Connection(con))
        
        df.to_csv('./log/ekko.csv', index=False, header=True)
        
        df_result.append(df)
        
    except BaseException as e:
        log.error(module, e.__str__())
        rc = cf.rc_error
    
    return rc

    
    
    """