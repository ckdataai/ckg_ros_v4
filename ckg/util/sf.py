# -*- coding: utf-8 -*-
"""
Created on Thu Jun  3 08:39:17 2021

@author: mike.zhang
"""

import sys, os

import requests

import ckg.conf.config as cf

def connect_sf():
    params = {
        "grant_type": "password",
        "client_id":  cf.sf_client_id, #  "3MVG9YDQS5WtC11omu71yf.NjJHZlyfHpF4fzPVPSBaSTNvFq8Qn2LgQp.wrnvPyR8SEU6eJ4FjBPLGD.a30y", # Consumer Key
        "client_secret": cf.sf_client_secret, #"64F433AA6F337189E12958E72C6DA8C9D7C7A9EF4B9D649854FA5EB13EBC38D1", # Consumer Secret
        "username": cf.sf_user, #"sfdc.integration@charleskeith.com.production", # The email you use to login
        "password": cf.sf_password, #"Cksfdc1688uwMJCD1mB359Xhkoi6Gv6qFB" # Concat your password and your security token
    }
    #r = requests.post("https://login.salesforce.com/services/oauth2/token", params=params)
    r = requests.post(cf.sf_url+"/services/oauth2/token", params=params)

    # if you connect to a Sandbox, use test.salesforce.com instead
    access_token = r.json().get("access_token")
    instance_url = r.json().get("instance_url")
    print("Access Token:", access_token)
    print("Instance URL", instance_url)
    
    return access_token, instance_url
    
def api_call(access_token, instance_url, action, parameters = {}, method = 'get', data = {}):
    """
    Helper function to make calls to Salesforce REST API.
    Parameters: action (the URL), URL params, method (get, post or patch), data for POST/PATCH.
    """
    headers = {
        'Content-type': 'application/json',
        'Accept-Encoding': 'gzip',
        'Authorization': 'Bearer %s' % access_token
    }
    if method == 'get':
        r = requests.request(method, instance_url+action, headers=headers, params=parameters, timeout=30)
    elif method in ['post', 'patch']:
        r = requests.request(method, instance_url+action, headers=headers, json=data, params=parameters, timeout=10)
    else:
        # other methods not implemented in this example
        raise ValueError('Method should be get or post or patch.')
    print('Debug: API %s call: %s' % (method, r.url) )
    if r.status_code < 300:
        if method=='patch':
            return r.json()
        else:
            return r.json()
    else:
        raise Exception('API error when calling %s : %s' % (r.url, r.content))
