# -*- coding: utf-8 -*-
"""
Created on Thu Jun  3 10:10:32 2021

@author: mike.zhang
"""
import sys
import os
from datetime import datetime as dt, timedelta
from dateutil import relativedelta
import json
from os.path import abspath

import ckg.conf.config as cf

def default_str(val):
    if val==None:
        return ''
    else:
        return str(val).strip()
    
#'%Y-%m-%d'
def get_datetime(fmt, delta):
    
    tmp = dt.today() - timedelta(delta)
    
    return tmp.strftime(fmt)

def get_datetime_ckc(fmt):
    
    
    tmp = dt.today()
    nxt = tmp
    if tmp.day > 15:
        nxt = tmp + relativedelta.relativedelta(months=1)
        nxt = nxt.replace(day=1)
        
    return nxt.strftime(fmt)

def get_week_start_date(fmt):
    today = dt.today()
    start = today - timedelta(days=today.weekday())
    
    return start.strftime(fmt)


def remove_trailing_zeros(s):

    if s == None:
        return s
    
    s = str(s)
    
    if s=='' or s==None:
        return s
    
    s1= s[::-1].strip('0')
    
    if s1 == '.':
        s1 = '0'
    elif s1[0]=='.':
        s1 = s1[1:]
        
    s1 = s1[::-1]    
    
    
    return s1

def read_user_conf():
    cf = None
    try:
        f = abspath('./')+'\\app.json'
        cf = json.loads(open(f, 'r').read())
    except:
        cf = None
    
    return cf

        

    
    