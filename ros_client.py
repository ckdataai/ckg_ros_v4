"""
    Program:      ros_main.py
    Function:     main program
    History:      2022.01.29        Mike           Start.

"""

import ckg.util.log as log
import ckg.util.util as util
import ckg.util.logic as logic
import ckg.data.data_util as data_util
import ckg.conf.config as cf
from ckg.model.ros_api import *
import sys

from flask import *

'''
    Module:       main program
    Function:     main program
    Usuage:       
                  $python ros_client.py country_list brand_list model_id launch_id
                 
                  e.g.
                  $python ros_client.py "SINGAPORE,MALAYSIA" "CK,PD" xgb E1
                  
                          
'''
if __name__ == "__main__":
    module = '__main__()'
    log.debug(module, 'Start.')
    
    print(sys.argv[1])

    country_list = sys.argv[1].split(',')
    brand_list = sys.argv[2].split(',')

    #cf.log_LEVEL = cf.log_ERROR
    
    api = ROSAPI(country_list, brand_list)
    rc = api.predict_by_launch(sys.argv[3], sys.argv[4])
    
    # if len(sys.argv) == 1:
    #     print(f"ros_main.py all|data|train [rf|lightboost|xgboost|nn]|deploy|clear")
    #     sys.exit(0)
        
    # if sys.argv[1] in 'data':
    #     log.debug(module, f"calling DataPreprocessing.main().")
    #     dp = data_util.DataPreprocessing()
    #     rc = dp.main()
    #     log.debug(module, f"called DataPreprocessing.main() returned {rc}.")
    
    # if sys.argv[1] in 'train':
    #     log.debug(module, f"argv[2]: {sys.argv[2]}")
    #     if sys.argv[2] in cf.models.keys():
    #         print(f"valid")
    #         model_class = globals()[cf.models[sys.argv[2]]]
    #         model_object = model_class(cf.latest_input_file)
    #         model_object.train()
    #         #model_object.predict(None)
            
    #     else:
    #         print(f"invalid")
    
    # app = Flask(__name__)
    
    # log.debug(module, 'Start.')
    # app.run()
            
    # log.debug(module, 'End.')
    
    
    
