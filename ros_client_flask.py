"""
    Program:      ros_main.py
    Function:     main program
    History:      2022.01.29        Mike           Start.

"""

import ckg.util.log as log
import ckg.util.util as util
import ckg.util.logic as logic
import ckg.data.data_util as data_util
import ckg.conf.config as cf
from ckg.model.ros_api import *
import sys

import requests as rq


'''
    Module:       main program
    Function:     main program
    Usuage:       
                  $python ros_client.py country_list brand_list model_id launch_id
                 
                  e.g.
                  $python ros_client.py "SINGAPORE,MALAYSIA" "CK,PD" xgb E1
                  
                          
'''
if __name__ == "__main__":
    module = '__main__()'
    log.debug(module, 'Start.')
    
    json_data={
        'data': []
        }
    
    one_country = {
        'country': 'SINGAPORE',
        'store': ['M500000140', 'M500000153'],
        'sku': [['CK1-70900369', 'BLUE_35'],
                ['CK1-70900369', 'BLUE_38'],
                ['CK1-61900001', 'PINK_41'],
                ['CK2-30781780-7', 'CREAM_XL']
                ]
        }
    json_data['data'].append(one_country)
    
    one_country = {
        'country': 'HONG KONG',
        'store': ['S000001269', 'S000001275'],
        'sku': [['CK1-70900369', 'BLUE_35'],
                ['CK1-70900369', 'BLUE_38'],
                ['CK1-61900001', 'PINK_41'],
                ['CK2-30781780-7', 'CREAM_XL']
                ]
        }
    json_data['data'].append(one_country)
    
        
    rs = rq.post('http://localhost:9688/predict', json=json_data)
    print(rs.json())
    
    
