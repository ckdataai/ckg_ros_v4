"""
    Program:      ros_main.py
    Function:     main program
    History:      2022.01.29        Mike           Start.

"""

import ckg.util.log as log
import ckg.util.util as util
import ckg.util.logic as logic
import ckg.data.data_util as data_util
import ckg.conf.config as cf
from ckg.model.ros_api import *
import sys

from flask import *

'''
    Module:       main program
    Function:     main program
    Usuage:       
                  $python ros_main.py all|data|train|deploy|clear
                 
                  where 
                     all         - execute clear, data, train, deploye
                     data        - prepare data only
                     train       - train the model
                     deploy      - deploy the model
                     clear       - clear all program generated files (log etc)
                          
'''

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():
    return 'Hallo John!'

@app.route('/predict', methods=['POST'])
def predict():
    request_data = request.get_json()
    print(request_data)
    
    return_data = []
    one_rec = ['CK1-1200', 2]
    return_data.append(one_rec)
    one_rec = ['CK1-1300', 2]
    return_data.append(one_rec)
    
    return jsonify(return_data)

if __name__ == "__main__":
    module = '__main__()'
    log.debug(module, 'Start.')
    
    app.run('0.0.0.0', port=9688, debug=True)
            
    log.debug(module, 'End.')
    
    
    
