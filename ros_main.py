"""
    Program:      ros_main.py
    Function:     main program
    History:      2022.01.29        Mike           Start.

"""

import ckg.util.log as log
import ckg.util.util as util
import ckg.util.logic as logic
import ckg.data.data_util as data_util
import ckg.conf.config as cf
from ckg.model.ros_models import *
import sys


'''
    Module:       main program
    Function:     main program
    Usuage:       
                  $python ros_main.py all|data|train|deploy|clear
                 
                  where 
                     all         - execute clear, data, train, deploye
                     data        - prepare data only for all countries
                     data "countries" - countries is country names separated by comma, no space, enclosed by double quotes,
                                     e.g
                                     data "SINGAPORE" to retrieve data for SINGAPORE only
                                     data "SINGAPORE,MALAYSIA,HONG KONG" is to treive data for SINGAPORE, MALAYSIA and HONG KONG
                     train       - train the model
                     train "countries" - countries is country names separated by comma, no space, enclosed by double quotes,
                                     e.g
                                     data "SINGAPORE" to retrieve data for SINGAPORE only
                                     data "SINGAPORE,MALAYSIA,HONG KONG" is to treive data for SINGAPORE, MALAYSIA and HONG KONG
                     deploy      - deploy the model
                     clear       - clear all program generated files (log etc)
                          
'''
if __name__ == "__main__":
    module = '__main__()'
    log.debug(module, 'Start.')
    
    if len(sys.argv) == 1:
        print(f"ros_main.py all|data|train [rf|lightboost|xgboost|nn]|deploy|clear")
        sys.exit(0)
        
    if sys.argv[1] in 'data':
        log.debug(module, f"calling DataPreprocessing.main().")
        dp = data_util.DataPreprocessing()
        country_list = []
        if len(sys.argv)>2:
            country_list = sys.argv[2].split(',')
            log.debug(module, f"country list: {country_list}")
        rc = dp.main(country_list)
        log.debug(module, f"called DataPreprocessing.main() returned {rc}.")
    
    if sys.argv[1] in 'train':
        log.debug(module, f"argv[2]: {sys.argv[2]}")
        if sys.argv[2] in cf.models.keys():
            print(f"valid")
            model_class = globals()[cf.models[sys.argv[2]]]
            
            country_list = []
            if len(sys.argv)>3:
                country_list = sys.argv[3].split(',')
                log.debug(module, f"country list: {country_list}")
            
            model_object = model_class(country_list)
            model_object.train()
            #model_object.predict(None)
            
        else:
            print(f"invalid")
            
    log.debug(module, 'End.')
    
    
    
