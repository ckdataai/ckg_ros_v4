"""
    Program:      ros_main.py
    Function:     main program
    History:      2022.01.29        Mike           Start.

"""

import ckg.util.log as log
import ckg.util.util as util
import ckg.util.logic as logic
import ckg.data.data_util as data_util
import ckg.conf.config as cf
from ckg.model.ros_models import *
import sys
import mysql.connector as con


'''
    Module:       main program
    Function:     main program
    Usuage:       
                  $python ros_main.py all|data|train|deploy|clear
                 
                  where 
                     all         - execute clear, data, train, deploye
                     data        - prepare data only
                     train       - train the model
                     deploy      - deploy the model
                     clear       - clear all program generated files (log etc)
                          
'''

default_appln = 'ML'
default_lkname = '__DEFAULT__'
default_lkval = '-1'


if __name__ == "__main__":
    module = '__main__()'
    log.debug(module, 'Start.')
    
    
    #retrieve all SKU attributes
    
    db = None
    cur  = None
    
    try:
        # mysql connection
        db = con.connect(
            host='rm-gs57fkcp61816itg3bo.mysql.singapore.rds.aliyuncs.com', #public
            #host ='rm-gs57fkcp61816itg369570.mysql.singapore.rds.aliyuncs.com',  #internal
            user='data_science',
            passwd='Data@Sc1ence20210617',
            database='data_science'
        )
        
        cur = db.cursor()
        
        #sap connection
        sap_con = sap.connect_to_sap()
        
        #-----------------------------------------------------------------------
        # get the article attributes
        #-----------------------------------------------------------------------
        df_list = []
        response = sap_con.call(
            'ZBAPI_CLAS',
            GT_CLASS=['CK_ARTICLE']
        )
        log.debug(module, '01.')
        df = pd.DataFrame(response['GT_ATTRIBUTES'])
        df['CLASS'] = 'Article'
        df_list.append(df)
        
        response = sap_con.call(
            'ZBAPI_CLAS',
            GT_CLASS=['CK_SKU']
        )
        log.debug(module, '02.')
        df = pd.DataFrame(response['GT_ATTRIBUTES'])
        df['CLASS'] = 'SKU'
        df_list.append(df)
        
        df = pd.concat(df_list)

        log.debug(module, '03.')
        print(df.columns)
        df = df.sort_values(by=['CLASS', 'ATTRIBUTE_NAME']).reset_index(drop=True)
        df.to_csv(cf.data_dst+'01_sap_attrs.csv', 
                  header=['Attr Type', 'Attr Name', 'Attr Desc'], 
                  columns=['CLASS', 'ATTRIBUTE_NAME', 'ATTRIBUTE_DESC'],
                  index=False)
        log.debug(module, '03a.')
        
        df_c_list = []
        for index, row in df.iterrows():
            
            gt_class = 'CK_ARTICLE'
            if row['CLASS'] == 'SKU':
                gt_class = 'CK_SKU'
            
            response = sap_con.call(
                'ZBAPI_CHAR',
                GT_CLASS=[gt_class],
                GT_CHAR_NAME=[row['ATTRIBUTE_NAME']]                
            )

            df_c = pd.DataFrame(response['GT_ATTRIBUTES'])
            if (df_c.shape[0] > 0):
                df_c.loc[df_c['CLASS'] == 'CK_ARTICLE', 'CLASS'] = 'Article'
                df_c.loc[df_c['CLASS'] == 'CK_SKU', 'CLASS'] = 'SKU'
                df_c_list.append(df_c)

        log.debug(module, '04.')
        df_c = pd.concat(df_c_list)
        df_c.sort_values(by=['ATTRIBUTE_NAME', 'ATTRIBUTE_VAL']).reset_index(drop=True)
        df_c.to_csv(cf.data_dst+'02_sap_attr_values.csv', 
                  header=['Attr Type', 'Attr Name', 'Attr Value', 'Attr Desc'], 
                  columns=['CLASS', 'ATTRIBUTE_NAME', 'ATTRIBUTE_VAL', 'ATTRIBUTE_DESC'],
                  index=False)
        
        attr_seq = 0
        for index, row in df.iterrows():
            # if index > 5:
            #     break
            
            attr_type = row[0]
            attr_name = row[1]
            log.debug(module, f"Attr Type: {attr_type}, Attr Name: {attr_name}")
            
            attr_df = df_c[(df_c['CLASS']==attr_type)&(df_c['ATTRIBUTE_NAME']==attr_name)]
            log.debug(module, f"data: \n{attr_df}")
            
            attr_df['ATTRIBUTE_VAL'] = attr_df['ATTRIBUTE_VAL'].apply(lambda x: x.strip())
            attr_df.sort_values(by=['ATTRIBUTE_VAL']).reset_index(drop=True)

            print(attr_df.columns)
            
            attr_seq = 0
            
            #- case 1: Free Text
            log.debug(module, f"shape: {attr_df.shape[0]}, first row 2nd field: {attr_df.iloc[0][2]}")
            if attr_df.shape[0] == 1 and attr_df.iloc[0][2]=='Free Text':
                log.debug(module, f"Yes, Free Text.")
                
                response = sap_con.call(
                    'ZBAPI_CHARACTERISTIC_VALUE',
                    IM_CHAR_NAME=[attr_name]                
                )
    
                tmp_df = pd.DataFrame(response['T_ATTRIBUTES'])
                if tmp_df.shape[0] > 0:
                    log.debug(module, f"name: {tmp_df.iloc[0][0]}, value: {tmp_df.iloc[0][1]}")
                else:
                    log.debug(module, f"no value, skip it.")
                    continue
                
                tmp_df['ATTRIBUTE_VAL'] = tmp_df['ATTRIBUTE_VAL'].apply(lambda x: x.strip())
                tmp_df.sort_values(by=['ATTRIBUTE_VAL']).reset_index(drop=True)
                tmp_df.drop_duplicates(inplace=True)
                tmp_df = tmp_df.reset_index(drop=True)
                
                
                #-always create a _DEFAULT_
                sql = '''
                        select count(*) from rf_lookup
                         where appln = %s
                           and lkid = %s
                           and lkname = %s
                        '''
                cur.execute(sql, (default_appln, attr_name, default_lkname))   
                r = cur.fetchone()
                if r[0] == 1:
                    log.debug(module, f"attr_name: {attr_name}, attr_val: [{default_lkname}] exists!")
                else:                    
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{default_lkname}|{default_lkval}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, 1, default_lkname,  '', '', '',  default_lkval)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
                    
                
                
                for i, r in tmp_df.iterrows():
                    attr_val  = r[1]
                    log.debug(module, f"attr_name: {attr_name}, attr_val: [{attr_val}]")
                    if attr_val == None or attr_val == '':
                        continue
                                                    
                    # attr_val got value 
                    else:
                        sql = '''
                                select count(*) from rf_lookup
                                 where appln = %s
                                   and lkid = %s
                                   and lkname = %s
                                '''
                        cur.execute(sql, (default_appln, attr_name, attr_val))   
                        r = cur.fetchone()
                        if r[0] == 1:
                            log.debug(module, f"attr_name: {attr_name}, attr_val: [{attr_val}] exists!")
                            continue
                            

                        sql = '''
                                select max(lkseq),max(cast(lkval as signed)) from rf_lookup
                                 where appln = %s
                                   and lkid = %s
                                '''
                        #print(sql)
                        cur.execute(sql, (default_appln, attr_name))    
                        r = cur.fetchone()
                        attr_seq = 0
                        lk_val = 0
                        log.debug(module, f"record(new): {r}, {r[0]}, {r[1]}")
                        if r[0] == None:
                            attr_seq = 1
                            lk_val = 1
                            log.debug(module, f"record(new.1): {attr_seq}, {lk_val}")
                        else:
                            attr_seq = r[0] + 1
                            if r[1] == -1:
                                lk_val = 1
                            else:
                                lk_val = r[1] + 1
                            log.debug(module, f"record(new.2): {attr_seq}, {lk_val}")
                                
                        # create default record
                        log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{attr_val}|{lk_val}]")
                        sql = '''
                                insert into rf_lookup values (
                                    %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                    )
                                  '''
                        rows = [
                                
                                (default_appln, attr_name, attr_seq, attr_val,  '', '', '',  lk_val)
                            
                                ]
                        
                        cur.executemany(sql, rows)
                        db.commit()

                    
            #- case 2: List Value (Color)
            elif attr_type == 'SKU' and attr_name == 'SKU_COLOR':
                #-----------------------------------------------------------------------
                # get the color
                #-----------------------------------------------------------------------
                response = sap_con.call(
                    'ZBAPI_COLORS'
                )
                df_color = pd.DataFrame(response['GT_COLORS'])
                log.debug(module, f"df_color.columns: {df_color.columns}")
                df_color = df_color.sort_values(by=['COLOR']).reset_index(drop=True)
                df_color.to_csv(cf.data_dst+'03_sap_color.csv', 
                          header=['SAP Code', 'SAP Value', 'Batch Color'], 
                          columns=['COLOR', 'COLOR_VALUE', 'BATCH_COLOR'],
                          index=False)

                #-always create a _DEFAULT_
                sql = '''
                        select count(*) from rf_lookup
                         where appln = %s
                           and lkid = %s
                           and lkname = %s
                        '''
                cur.execute(sql, (default_appln, attr_name, default_lkname))   
                r = cur.fetchone()
                if r[0] == 1:
                    log.debug(module, f"attr_name: {attr_name}, attr_val: [{default_lkname}] exists!")
                else:                    
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{default_lkname}|{default_lkval}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, 1, default_lkname,  '', '', '',  default_lkval)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
                                    
                for index, row in df_color.iterrows():
                    color_id      = row[0]
                    color_val     = row[1]
                    color_batch   = row[2]
                    
                    log.debug(module, f"color, id/val/batch: [{color_id}|{color_val}|{color_batch}]")
                    sql = '''
                            select count(*) from rf_lookup
                             where appln = %s
                               and lkid = %s
                               and lkname = %s
                            '''
                    cur.execute(sql, (default_appln, attr_name, color_val))   
                    r = cur.fetchone()
                    if r[0] == 1:
                        log.debug(module, f"attr_name: {attr_name}, color_val: [{color_val}] exists!")
                        continue
                        

                    sql = '''
                            select max(lkseq),max(cast(lkval as signed)) from rf_lookup
                             where appln = %s
                               and lkid = %s
                            '''
                    #print(sql)
                    cur.execute(sql, (default_appln, attr_name))    
                    r = cur.fetchone()
                    attr_seq = 0
                    lk_val = 0
                    log.debug(module, f"record(new): {r}, {r[0]}, {r[1]}")
                    if r[0] == None:
                        attr_seq = 1
                        lk_val = 1
                        log.debug(module, f"record(new.1): {attr_seq}, {lk_val}")
                    else:
                        attr_seq = r[0] + 1
                        if r[1] == -1:
                            lk_val = 1
                        else:
                            lk_val = r[1] + 1
                        log.debug(module, f"record(new.2): {attr_seq}, {lk_val}")
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{color_val}|{lk_val}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, attr_seq, color_val,  color_id, color_batch, '',  lk_val)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
                
                    
            #- case 3: List Value (Size)
            elif attr_type == 'SKU' and attr_name == 'SKU_SIZE':
                #-----------------------------------------------------------------------
                # get the color
                #-----------------------------------------------------------------------
                response = sap_con.call(
                    'ZBAPI_SIZES'
                )
                df_size = pd.DataFrame(response['GT_SIZES'])
                log.debug(module, f"df_size.columns: {df_size.columns}")
                df_size = df_size.sort_values(by=['SIZE_CODE']).reset_index(drop=True)
                df_size.to_csv(cf.data_dst+'04_sap_size.csv', 
                                  header=['SAP Code', 'SAP Value', 'Batch Size'], 
                                  columns=['SIZE_CODE', 'SIZE_VALUE', 'BATCH_SIZE'],
                                  index=False)

                #-always create a _DEFAULT_
                sql = '''
                        select count(*) from rf_lookup
                         where appln = %s
                           and lkid = %s
                           and lkname = %s
                        '''
                cur.execute(sql, (default_appln, attr_name, default_lkname))   
                r = cur.fetchone()
                if r[0] == 1:
                    log.debug(module, f"attr_name: {attr_name}, attr_val: [{default_lkname}] exists!")
                else:                    
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{default_lkname}|{default_lkval}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, 1, default_lkname,  '', '', '',  default_lkval)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
                                    
                for index, row in df_size.iterrows():
                    size_id      = row[0]
                    size_val     = row[1]
                    size_batch   = row[2]
                    
                    log.debug(module, f"color, id/val/batch: [{size_id}|{size_val}|{size_batch}]")
                    sql = '''
                            select count(*) from rf_lookup
                             where appln = %s
                               and lkid = %s
                               and lkname = %s
                            '''
                    cur.execute(sql, (default_appln, attr_name, color_val))   
                    r = cur.fetchone()
                    if r[0] == 1:
                        log.debug(module, f"attr_name: {attr_name}, size_val: [{size_val}] exists!")
                        continue
                        

                    sql = '''
                            select max(lkseq),max(cast(lkval as signed)) from rf_lookup
                             where appln = %s
                               and lkid = %s
                            '''
                    #print(sql)
                    cur.execute(sql, (default_appln, attr_name))    
                    r = cur.fetchone()
                    attr_seq = 0
                    lk_val = 0
                    log.debug(module, f"record(new): {r}, {r[0]}, {r[1]}")
                    if r[0] == None:
                        attr_seq = 1
                        lk_val = 1
                        log.debug(module, f"record(new.1): {attr_seq}, {lk_val}")
                    else:
                        attr_seq = r[0] + 1
                        if r[1] == -1:
                            lk_val = 1
                        else:
                            lk_val = r[1] + 1
                        log.debug(module, f"record(new.2): {attr_seq}, {lk_val}")
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{size_val}|{lk_val}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, attr_seq, color_val,  color_id, color_batch, '',  lk_val)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
                    
            #- case 4: Other list vlaues
            else:
                log.debug(module, f"case 4, other lists.")
                #['CLASS', 'ATTRIBUTE_NAME', 'ATTRIBUTE_VAL', 'ATTRIBUTE_DESC'
                attr_df.to_csv('./log/05_'+attr_name+'.csv', 
                          header=['ATTRIBUTE_NAME', 'ATTRIBUTE_VAL', 'ATTRIBUTE_DESC'],
                          columns=['ATTRIBUTE_NAME', 'ATTRIBUTE_VAL', 'ATTRIBUTE_DESC'],
                          index=False)

                #-always create a _DEFAULT_
                sql = '''
                        select count(*) from rf_lookup
                         where appln = %s
                           and lkid = %s
                           and lkname = %s
                        '''
                cur.execute(sql, (default_appln, attr_name, default_lkname))   
                r = cur.fetchone()
                if r[0] == 1:
                    log.debug(module, f"attr_name: {attr_name}, attr_val: [{default_lkname}] exists!")
                else:                    
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{default_lkname}|{default_lkval}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, 1, default_lkname,  '', '', '',  default_lkval)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
                                    
                for index, row in attr_df.iterrows():
                    attr_val     = row[2]
                    attr_dsc     = row[3]
                    
                    log.debug(module, f"color, attr name/val/dsc: [{attr_name}|{attr_val}|{attr_dsc}]")
                    sql = '''
                            select count(*) from rf_lookup
                             where appln = %s
                               and lkid = %s
                               and lkname = %s
                            '''
                    cur.execute(sql, (default_appln, attr_name, attr_dsc))   
                    r = cur.fetchone()
                    if r[0] == 1:
                        log.debug(module, f"attr_name: {attr_name}, attr_dsc: [{attr_dsc}] exists!")
                        continue
                        

                    sql = '''
                            select max(lkseq),max(cast(lkval as signed)) from rf_lookup
                             where appln = %s
                               and lkid = %s
                            '''
                    #print(sql)
                    cur.execute(sql, (default_appln, attr_name))    
                    r = cur.fetchone()
                    attr_seq = 0
                    lk_val = 0
                    log.debug(module, f"record(new): {r}, {r[0]}, {r[1]}")
                    if r[0] == None:
                        attr_seq = 1
                        lk_val = 1
                        log.debug(module, f"record(new.1): {attr_seq}, {lk_val}")
                    else:
                        attr_seq = r[0] + 1
                        if r[1] == -1:
                            lk_val = 1
                        else:
                            lk_val = r[1] + 1
                        log.debug(module, f"record(new.2): {attr_seq}, {lk_val}")
                            
                    # create default record
                    log.debug(module, f"insert: [{default_appln}|{attr_name}|{attr_seq}|{attr_dsc}|{lk_val}]")
                    sql = '''
                            insert into rf_lookup values (
                                %s,%s,%s,%s,%s,%s,%s,%s,sysdate(),user(),sysdate(),user()
                                )
                              '''
                    rows = [
                            
                            (default_appln, attr_name, attr_seq, attr_dsc,  attr_val, '', '',  lk_val)
                        
                            ]
                    
                    cur.executemany(sql, rows)
                    db.commit()
            
            
            
            db.commit() 
        
        #-----------------------------------------------------------------------
        # get the color
        #-----------------------------------------------------------------------
        df_list = []
        response = sap_con.call(
            'ZBAPI_COLORS'
        )
        df = pd.DataFrame(response['GT_COLORS'])
        df = df.sort_values(by=['COLOR_VALUE']).reset_index()
        df.to_csv(cf.data_dst+'03_sap_color.csv', 
                  header=['SAP Code', 'SAP Value', 'Batch Color'], 
                  columns=['COLOR', 'COLOR_VALUE', 'BATCH_COLOR'],
                  index=False)

        #-----------------------------------------------------------------------
        # get the size
        #-----------------------------------------------------------------------
        df_list = []
        response = sap_con.call(
            'ZBAPI_SIZES'
        )
        log.debug(module, '01.')
        df = pd.DataFrame(response['GT_SIZES'])
        df = df.sort_values(by=['SIZE_CODE']).reset_index()
        df.to_csv(cf.data_dst+'04_sap_size.csv', 
                  header=['SAP Code', 'SAP Value', 'Batch Size'], 
                  columns=['SIZE_CODE', 'SIZE_VALUE', 'BATCH_SIZE'],
                  index=False)


    
        cur.close()
        db.commit()
                
        log.debug(module, 'End.')

    except BaseException as e:
        log.error(module, e.__str__())
        try:
            db.rollback()
            cur.close()
        except:
            pass
    
    
    
    
